/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button").addEventListener("click", selectKapal);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getListKapal() {
  localStorage.setItem('mode_report','0');
  var selbox = '<select class="form-control" id="id_kapal">';
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/kapal", {} ).done( function( res ) {
    //SpinnerPlugin.activityStart("Loading Data Kapal");
    $.each( res.result, function( i, item ) {
      var count = 0;
      var id_kapal;
      var nama_kapal;
      $.each( this, function( j, item2 ) {
        if(count == 0)
          nama_kapal = item2;
        else 
          id_kapal = item2;
        count++;
      } );
      selbox += '<option value="' + id_kapal + '">' + nama_kapal + '</option>';
    } );
    selbox += '</select>';
    $('#list_kapal').html("");
    $(selbox).appendTo('#list_kapal');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function selectKapal() {
  var id_kapal = $('#id_kapal').val();
  localStorage.setItem('id_kapal', id_kapal);
  window.location.href = "buat_survey.html";
}