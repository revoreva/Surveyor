/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button").addEventListener("click", selectItem);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  window.location.href = "engine_item.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getData() {
  //SpinnerPlugin.activityStart("Get Data...");
  $('#nama_machine_engine').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + localStorage.getItem('engine_nama'));
  var machinery_id = localStorage.getItem('machinery_id');
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/machine/engine/" + machinery_id, {} ).done( function( res ) {
    $.each( res.result, function( i, item ) {
      var count = 0;
      var id;
      var nama;
      var ok = 0;
      $.each( this, function( j, item2 ) {
        if(count == 0 && item2 == localStorage.getItem('engine_item_id'))
          ok = 1;
        else if(count == 2 && ok == 1){
          $('#nama_engine_next').html(item2);
          return false;
        }
        count++;
      } );
      if(ok) return false;
      //SpinnerPlugin.activityStop();
      $('#myModal').modal('hide');
    } );
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
  
  var selbox = '<select class="form-control" id="label_id">';
  var engine_item_id = localStorage.getItem('engine_item_id');
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/machine/engine/" + machinery_id + '/' + engine_item_id, {} ).done( function( res ) {
    if(res.message == "success"){
      $.each( res.result, function( i, item ) {
        var count = 0;
        var id;
        var nama;
        $.each( this, function( j, item2 ) {
          if(count == 0)
            id = item2;
          else if(count == 1){
            nama = item2;
          }
          count++;
        } );
        selbox += '<option value="' + id + '">' + nama + '</option>';
      } );
    }
    selbox += '</select>';
    $('#list_item').html("");
    $(selbox).appendTo('#list_item');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function selectItem() {
  var label_id = $('#label_id').val();
  localStorage.setItem('label_id', label_id);
  var nama = $("#label_id option[value='" + label_id + "']").text();
  localStorage.setItem('nama_label_engine',nama);
  var id = localStorage.getItem('engine_item_id');
  localStorage.setItem('last_engine','engine_next.html');
  if(id == 1 || id == 2 || id == 4 || id == 7 || id == 8 || id == 10) window.location.href = "form_engine_next.html";
  else if(id == 3 || id == 9) window.location.href = "form_bahan_next.html";
  else window.location.href = "form_indi_uji_next.html";
}