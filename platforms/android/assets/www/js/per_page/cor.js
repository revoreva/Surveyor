/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("back").addEventListener("click", back);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back(){
  window.location.href = "survey_list.html";
}

function logout() {
  //localStorage.removeItem('item');
  localStorage.clear();
  window.location.href = "index.html";
}

function getCOR() {
  getListCertificates();
  getListDocuments();
}

function getListCertificates() {
  var selbox = '<select class="form-control" id="id_certificate" onchange="goToCertificate(this)">';
  $('#myModal').modal('show');
  $.getJSON( url + "/certificates", {
  } ).done( function( res ) {
    //SpinnerPlugin.activityStart("Mengunduh Data List Certificate & Document...");
    selbox += '<option value="0" disabled selected>Select The Item Below..</option>';
    if(res.message == "success"){
      $.each( res.result, function( i, item ) {
        var count = 0;
        var id;
        var nama;
        $.each( this, function( j, item2 ) {
          if(count == 0)
            id = item2;
          else 
            nama = item2;
          count++;
          //alert(selbox);
        } );
        selbox += '<option value="' + id + '">' + nama + '</option>';
      } );
    }
    selbox += '</select>';
    $('#certificateList').html("");
    $(selbox).appendTo('#certificateList');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function getListDocuments() {
  $('#myModal').modal('show');
  var selbox = '<select class="form-control" id="id_document" onchange="goToDocument(this)">';
  //SpinnerPlugin.activityStart("Get list documents...");
  $.getJSON( url + "/documents", {
  } ).done( function( res ) {
     selbox += '<option value="-" disabled selected>Select The Item Below..</option>';
    $.each( res.result, function( i, item ) {
      var count = 0;
      var id;
      var nama;
      
      $.each( this, function( j, item2 ) {
        if(count == 0)
          id = item2;
        else 
          nama = item2;
        count++;
      } );
      selbox += '<option value="' + id + '">' + nama + '</option>';
    } );
    selbox += '</select>';
    $('#documentList').html("");
    $(selbox).appendTo('#documentList');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function goToCertificate(element) {
  var id = element.value;
  var nama = element.options[element.selectedIndex].text;
  localStorage.setItem('id_certificate',id);
  localStorage.setItem('name_certificate',nama);
  window.location.href = "certificate.html";
}

function goToDocument(element) {
  var id = element.value;
  var nama = element.options[element.selectedIndex].text;
  localStorage.setItem('id_document',id);
  localStorage.setItem('name_document',nama);
  window.location.href = "document.html";
}