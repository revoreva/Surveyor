/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button").addEventListener("click", selectItem);
  document.getElementById("back").addEventListener("click", back);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getData() {
  $('#nama_engine').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + localStorage.getItem('engine_nama'));
  var selbox = '<table class="table table-alpha"><tr><td colspan="2"><b>Motor Induk</b></td></tr>';
  var atribut;
  var flag = 0;
  //SpinnerPlugin.activityStart("Get Data...");
  var engine_id = localStorage.getItem('engine_id');
  var id_kapal = localStorage.getItem('id_kapal');
  $('#myModal').modal('show');
  $.getJSON( url + "/machine/" + engine_id + '/' + id_kapal, {
  } ).done( function( res ) {
    if(res.message == "success"){
      $.each( res.result, function( i, item ) {
        $.each( this, function( j, item2 ) {
          if(j.substring(0,1) == 'g' && !flag){
            selbox += '<tr><td colspan="2" style="padding-top: 10px;"><b>Gearbox</b></td></tr>';
            flag = 1;
          }
          atribut = j.substring(j.indexOf("_")+1);
          if(atribut != 'id' && atribut != 'imo' && atribut != 'nama') selbox += '<tr><td width="50%">' + atribut + '</td><td width="50%">: ' + item2 + '</td></tr>';
        } );
      } );
    } else {
      selbox += '<tr><td colspan="2" style="padding-top: 10px;"><b>Gearbox</b></td></tr>';
    }
    selbox += '</tr></table>';
    $('#list').html("");
    $(selbox).appendTo('#list');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  });
}

function selectItem() {
  window.location.href = "engine_item.html";
}

function back() {
  window.location.href = "menu_machinery.html";
}