/* global variables */
var url1 = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var url2 = "http://shipconditionsurvey.heliohost.org/surveykapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  }
  // localStorage.setItem('last_engine_item','superstructure');
}

function back() {
  window.location.href = "menu_machinery.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function show_image(url) {
  $("#imgModal").attr("src",url);
  // alert(url);
  // document.getElementById('imgModal').style.display = 'none';
  // document.getElementById('imgModal').style.display = 'block';
  // $('#imgModal').html("<img style=\"max-height:500px;max-width:500px;\" src=\""+ url + "\">");
  $('#gambarModal').modal('show');
}

function view_normal(min, max, str) {
  alert(str + ' normal:\nmin: ' + min + '\nmax: ' + max);
}

function update(engine_item_id, e_i_s_id, label_id, nama, kondisi, suhu, pressure, keterangan, gambar) {
  //engine_item_id, e_i_s_id, label_id, nama, kondisi, suhu, pressure, keterangan, gambar
  //e_i_s_id = param[counterr].e_i_s_id
  // engine_item_id = engine_item[i].engine_item_id
  // label_id = engine_item_label[i][j].engine_label_id
  // alert(id + ' ' + nama + ' ' + kondisi);
  localStorage.setItem('engine_item_id', engine_item_id);
  localStorage.setItem('e_i_s_id', e_i_s_id);
  localStorage.setItem('label_id', label_id);
  localStorage.setItem('nama_label_engine',nama);
  localStorage.setItem('report_kondisi', kondisi);
  localStorage.setItem('report_suhu', suhu);
  localStorage.setItem('report_pressure', pressure);
  localStorage.setItem('report_keterangan', keterangan);
  localStorage.setItem('report_gambar', gambar);
  var id = localStorage.getItem('engine_item_id');
  if(localStorage.getItem('machinery_id') == 3){
    if(id >= 13 && id <= 17) window.location.href = "form_engine_next.html";
    else{
      if(label_id == 97 || label_id == 99 || label_id == 107 || label_id == 108 || label_id == 111 || label_id == 112)
        window.location.href = "form_engine_next.html";
      else 
        window.location.href = "form_indi_uji_next.html";
    }
  } else {
    if(id == 1 || id == 2 || id == 4 || id == 7 || id == 8 || id == 10)
    window.location.href = "form_engine_next.html";
    else if(id == 3 || id == 9)
      window.location.href = "form_bahan_next.html";
    else
      window.location.href = "form_indi_uji_next.html";
  }
}

function getData() {
  $('#gambarModal').on('hidden', function () {
    $("#imgModal").attr("src",'img/loading.gif');
  })
  namama = "Machinery Report";
  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);
  var machinery_id = localStorage.getItem('machinery_id');
  var engine_id = localStorage.getItem('engine_id');
  var id_kapal = localStorage.getItem('id_kapal');
  var id_survey = localStorage.getItem('id_survey_last');

  var selbox = '<div class="table table-alpha" style="width: 100%;">';
  var count = 0;
  var engine_item, engine_item_label, param, datanya;
  $('#myModal').modal('show');
  $.ajax({
    type: "GET",
    url: url2 + "/report/engine2_json/" + machinery_id + '/' + engine_id + '/' + id_kapal + '/' + id_survey,
    dataType: "text",  
    cache:false,
    success: function (response) {
      engine_item = $.parseJSON(response);
      engine_item_label = engine_item.engine_item_label;
      param = engine_item.param;
      datanya = engine_item.datanya;
      engine_item = engine_item.engine_item;
      console.log('success: engine_item:');
      // console.log(parameter);
      console.log(engine_item);
      console.log(url2 + "/report/engine2_json/" + machinery_id + '/' + engine_id + '/' + id_kapal + '/' + id_survey);
      $('#myModal').modal('hide');
    },
    error: function (ErrorResponse) {
      alert('Failed to Load Data from server!');
      engine_item = $.parseJSON('{"param":["1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[],"8":[],"9":[],"10":[],"11":[],"12":[],"13":[],"14":[]],"engine_item_label":{"1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[],"8":[],"9":[],"10":[],"11":[],"12":[],"13":[],"14":[]},"datanya":{"1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[],"8":[],"9":[],"10":[],"11":[],"12":[],"13":[],"14":[]},"engine_item":{"1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[],"8":[],"9":[],"10":[],"11":[],"12":[],"13":[],"14":[]}}');
      engine_item_label = engine_item.engine_item_label;
      param = engine_item.param;
      datanya = engine_item.datanya;
      engine_item = engine_item.engine_item;
      console.log('error');
      console.log(engine_item);
      // engine_item = engine_item;
      $('#myModal').modal('hide');
    }
  }).done(function () {
    var flagC = 1; 
    var konter = 0;
    // $('#myModal').modal('show');
    selbox += '<ul class="nav nav-tabs">';
    for (var i = 0; i < engine_item.length; i++) {
      if(flagC){
        selbox += '<li class="active">';
        flagC = 0;
      } else {
        selbox += '<li>';
      }
      selbox += "<a href='#tab_" + engine_item[i].engine_item_id + "' data-toggle='tab'>";
      selbox += engine_item[i].engine_item_nama + '</a>';
      selbox += '</li>';
    }
    selbox += '</ul>';
    selbox += '<div class="tab-content" style="width:100%;">';
    var flagD = 1;
    var counterr = 0;
    var counter2 = 0;
    var str;
    for (var i = 0; i < engine_item_label.length; i++) {
      if(flagD){
        str = "active in";
        flagD = 0;
      } else {
        str = "";
      }
      counter = 0;
      selbox += '<div class="tab-pane fade ' + str + '" id="tab_' + engine_item_label[i][counter].engine_item_id + '" style="overflow-x: auto;">';
      selbox += '<table class="table table-alpha table-bordered" id="table_' + engine_item_label[i][counter].engine_item_id + '">';
      selbox += '<thead><tr>';
      selbox += '<th style="text-align:center; vertical-align:middle;">Item</th>';
      selbox += '<th style="text-align:center; vertical-align:middle;">Kondisi</th>';
      selbox += '<th style="text-align:center; vertical-align:middle;">Suhu</th>';
      selbox += '<th style="text-align:center; vertical-align:middle;">Pressure</th>';
      selbox += '<th style="text-align:center; vertical-align:middle;">Keterangan</th>';
      selbox += '<th style="text-align:center; vertical-align:middle;">Gambar</th>';
      selbox += '<th style="text-align:center; vertical-align:middle;">Update</th>';
      selbox += '</tr></thead>';
      selbox += '<tbody>';
      for (var j = 0; j < engine_item_label[i].length; j++) {
        selbox += '<tr id = "tr_' + engine_item_label[i][j].engine_label_id + '"">';
        selbox += '<td> ' + engine_item_label[i][j].engine_label_nama + '</td>';
        
        if (param[counterr] != undefined && param[counterr].length > 0 && datanya[counterr] != undefined && datanya[counterr].length > 0) {
          console.log('param[' + counterr + ']');
          console.log(param[counterr]);
          console.log('datanya[' + counterr + ']');
          console.log(datanya[counterr]);
          if(param[counterr].e_i_s_id == datanya[counterr].e_i_s_id)
          {
            if(datanya[counterr][0].e_i_s_d_kondisi != 'Good')
              selbox += '<td style="text-align:center; vertical-align:middle;" bgcolor="#ff4d4d"> ' + datanya[counterr][0].e_i_s_d_kondisi + '</td>';
            else
              selbox += '<td style="text-align:center; vertical-align:middle;"> ' + datanya[counterr][0].e_i_s_d_kondisi + '</td>';

            if(datanya[counterr][0].e_i_s_d_suhu == 0 && datanya[counterr][0].e_i_s_d_pressure == 0)
              selbox += '<td style="text-align:center; vertical-align:middle;"> - </td>';
            else if (datanya[counterr][0].e_i_s_d_suhu < param[counterr][0].e_i_s_suhu_min || datanya[counterr][0].e_i_s_d_suhu > param[counterr][0].e_i_s_suhu_max)
            {
              selbox += '<td style="text-align:center; vertical-align:middle;" bgcolor="#ff4d4d" onclick="view_normal(' + param[counterr][0].e_i_s_suhu_min + ',' + param[counterr][0].e_i_s_suhu_max + ',\'Suhu\'' + ')"><a href="#" style="color: black; text-decoration: underline;">' + datanya[counterr][0].e_i_s_d_suhu + '</a></td>';
            }
            else selbox += '<td style="text-align:center; vertical-align:middle;"> ' + datanya[counterr][0].e_i_s_d_suhu + '</td>';

            if(datanya[counterr][0].e_i_s_d_suhu == 0 && datanya[counterr][0].e_i_s_d_pressure == 0)
              selbox += '<td style="text-align:center; vertical-align:middle;"> - </td>';
            else if (datanya[counterr][0].e_i_s_d_pressure < param[counterr][0].e_i_s_pressure_min || datanya[counterr][0].e_i_s_d_pressure > param[counterr][0].e_i_s_pressure_max) 
            {
              selbox += '<td style="text-align:center; vertical-align:middle;" bgcolor="#ff4d4d" onclick="view_normal(' + param[counterr][0].e_i_s_pressure_min + ',' + param[counterr][0].e_i_s_pressure_max + ',\'Pressure\'' + ')"><a href="#" style="color: black; text-decoration: underline;">' + datanya[counterr][0].e_i_s_d_pressure + '</a></td>';
            }
            else selbox += '<td style="text-align:center; vertical-align:middle;"> ' + datanya[counterr][0].e_i_s_d_pressure + '</td>';

            selbox += "<td align='center' style='vertical-align:middle;'>" + datanya[counterr][0].e_i_s_d_keterangan + "</td>";

            selbox += "<td style='text-align:center; vertical-align:middle;' onclick=\"show_image(\'" + datanya[counterr][0].e_i_s_d_gambar + "\')\" align=\"center\"><a href=\"#\">Lihat Gambar</a></td>";

            //engine_item_id, e_i_s_id, label_id, nama, kondisi, suhu, pressure, keterangan, gambar
            //e_i_s_id = param[counterr].e_i_s_id
            // engine_item_id = engine_item[i].engine_item_id
            // label_id = engine_item_label[i][j].engine_label_id
            // alert(id + ' ' + nama + ' ' + kondisi);
            selbox += '<td style="text-align:center; vertical-align:middle;"><input type="submit" class="btn btn-primary" value="update" onclick="update(';
            selbox += '\'' + engine_item[i].engine_item_id + '\', ';
            selbox += '\'' + param[counterr][0].e_i_s_id + '\', ';
            selbox += '\'' + engine_item_label[i][j].engine_label_id + '\', ';
            selbox += '\'' + engine_item_label[i][j].engine_label_nama + '\', ';
            selbox += '\'' + datanya[counterr][0].e_i_s_d_kondisi + '\', ';
            selbox += '\'' + datanya[counterr][0].e_i_s_d_suhu + '\', ';
            selbox += '\'' + datanya[counterr][0].e_i_s_d_pressure + '\', ';
            selbox += '\'' + datanya[counterr][0].e_i_s_d_keterangan + '\', ';
            selbox += '\'' + datanya[counterr][0].e_i_s_d_gambar + '\'';
            selbox += ')"></td>';
          }
        }
        else{
          selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
          selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
          selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
          selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
          selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
          selbox += '<td style="text-align:center; vertical-align:middle;"><input type="submit" class="btn btn-primary" value="update" onclick="update(';
          selbox += '\'' + engine_item[i].engine_item_id + '\', ';
          selbox += '\'-\', ';
          selbox += '\'' + engine_item_label[i][j].engine_label_id + '\', ';
          selbox += '\'' + engine_item_label[i][j].engine_label_nama + '\', ';
          selbox += '\'-\', ';
          selbox += '\'-\', ';
          selbox += '\'-\', ';
          selbox += '\'-\', ';
          selbox += '\'-\'';
          selbox += ')"></td>';
        }
        // var_dump(param);
        selbox += '</tr>';
        counterr++;
      }
      selbox += '</tbody>';
      selbox += '</table>';
      selbox += '</div>';
      counter++;
    }
    selbox += '</div>';
      // }
    // }

    
    $('#list_item').html("");
    $(selbox).appendTo('#list_item');
    // alert(selbox);
    $('#myModal').modal('hide');
    /*$.getJSON( url1 + "/machine/engine/" + machinery_id, {
      } ).done( function( res ) {
        // console.log(res);
        if(res.message == "success"){
          var count = 8;
          var konter = 0;
          selbox += '<ul class="nav nav-tabs">';
          var awal = 1;
          $.each( res.result, function( i, item ) {
            var countLL = 0;
            var id; //engine_item_id
            var nama;
            $.each( this, function( j, item2 ) {
              if(countLL == 0)
                id = item2;
              else if(countLL == 2)
                nama = item2;
              countLL++;
              //alert(selbox);
            } );
            // var nama_asli = nama;
            // nama = nama.replace(" ", "_");
            selbox += '<li';
            if(awal){
              selbox += ' class="active"';
              awal = 0;
            }
            selbox += '><a data-toggle="tab" href="#page' + id + '">' + nama + '</a></li>';            count++;
          } );
          selbox += '</ul>';
          selbox += '<div class="tab-content" style="width:100%;">';
          awal = 1;
          count = 0;
          var counterr = 0;
          $.each( res.result, function( i, item ) 
          {
            var countLL = 0;
            var id;
            var nama;
            $.each( this, function( j, item2 ) {
              if(countLL == 0)
                id = item2;
              else if(countLL == 2)
                nama = item2;
              countLL++;
              //alert(selbox);
            } );
            // nama = nama.replace(" ", "_");
            selbox += '<div id="page' + id + '" class="tab-pane fade in';
            if(awal){
              selbox += ' active';
              awal = 0;
            }
            selbox += '" style="width:100%;">';
            
            selbox += '<table class="table table-alpha">';
            selbox += '<tr>';
            selbox += '<th style="text-align:center">Item</th>';
            selbox += '<th style="text-align:center">Kondisi</th>';
            selbox += '<th style="text-align:center">Suhu</th>';
            selbox += '<th style="text-align:center">Pressure</th>';
            selbox += '<th style="text-align:center">Keterangan</th>';
            selbox += '<th style="text-align:center">Gambar</th>';
            selbox += '<th style="text-align:center">Update</th>';
            selbox += '</tr>';
            
            console.log('engine_item_label');
            console.log(engine_item_label[count]);
            var sizeLabel = engine_item_label[count].length;
            
            for(var i = 0; i < sizeLabel; i++){
              var flag = 0;
              selbox += '<tr>';
              selbox += '<td>' + counterr + '-=-' + engine_item_label[count][i].engine_label_nama + '</td>';
              
              console.log('engine_item');
              console.log(engine_item);
              
              if(datanya[counterr] != undefined && param[counterr] != undefined){
                console.log('param[' + counterr + ']');
                console.log(param[counterr]);
                console.log('datanya[' + counterr + ']');
                console.log(datanya[counterr]);
              } else {
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">update</td>';
              }
              //for tiap data
              
              if(param[counterr][0].e_i_s_id == datanya[counterr][0].e_i_s_id){
                if(datanya[counterr][0].e_i_s_d_kondisi != 'Good')
                  selbox += '<td bgcolor="#ff4d4d"> ' + datanya[counterr][0].e_i_s_d_kondisi + '</td>';
                else
                  selbox += '<td> ' + datanya[counterr][0].e_i_s_d_kondisi + '</td>';
              }

              selbox += '</tr>';
              counterr++;
            }
            
            selbox += '</table>';
            selbox += '</div>';
            count++;
          });
          
          selbox += '</div>';
          // selbox += '</td></tr>';
          selbox += '</div>';
          // selbox += '<tr><th style="text-align:center">Item</th>';
          // selbox += '<th style="text-align:center">Kondisi</th>';
          // selbox += '<th style="text-align:center">Frame</th>';
          // selbox += '<th style="text-align:center">Ketebalan</th>';
          // selbox += '<th style="text-align:center">Keterangan</th>';
          // selbox += '<th style="text-align:center">Gambar</th>';
          // selbox += '<th style="text-align:center">Update</th>';
          // selbox += '</tr>';
          $('#list_item').html("");
          $(selbox).appendTo('#list_item');
          // alert(selbox);
          $('#myModal').modal('hide');
          //SpinnerPlugin.activityStop();
        } else {
          alert('Failed to Load Data from server!');
          $('#myModal').modal('hide');
        }
      }).fail(function () {
        alert('Failed to Load Data from server!');
        $('#myModal').modal('hide');
      });
    });*/
  });
}

function selectItem() {
  
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}