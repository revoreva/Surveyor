/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button").addEventListener("click", selectItem);
  document.getElementById("back").addEventListener("click", back);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getData() {
  var selbox = '<select class="form-control" id="item_id" onchange="getName(this)">';
  selbox += '<option value="0" disabled selected>Select Data</option>';
  //SpinnerPlugin.activityStart("Get Data...");
  var id_kapal = localStorage.getItem('id_kapal');
  $('#myModal').modal('show');
  console.log(url + "/machine/" + id_kapal);
  $.getJSON( url + "/machine/" + id_kapal, {
  } ).done( function( res ) {
    $.each( res.result, function( i, item ) {
      var id, id2, nama;
      $.each( this, function( j, item2 ) {
        if(j == 'machinery_id')
          id = item2;
        else if(j == 'engine_id')
          id2 = item2;
        else if(j == 'machinery_nama')
          nama = item2;
      } );
      selbox += '<option value="' + id + ';' + id2 + '">' + nama + '</option>';
    } );
    selbox += '</select>';
    $('#list_item').html("");
    $(selbox).appendTo('#list_item');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function selectItem() {
  var idAll = $('#item_id').val().split(";");
  var id = idAll[0], id2 = idAll[1];
  localStorage.setItem('engine_id', id2);
  localStorage.setItem('machinery_id', id);
  if(localStorage.getItem('mode_report') == '1'){
    window.location.href = "menu_machinery_report.html";
  }
  else if(id == 3) window.location.href = "peralatan_item.html";
  else window.location.href = "engine.html";
}

function getName(element) {
  localStorage.setItem('engine_nama',element.options[element.selectedIndex].text);
}

function back() {
  window.location.href = "survey_list.html";
}