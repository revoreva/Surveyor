/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("submit").addEventListener("click", submit);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  window.location.href = "form_outfitting.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function submit() {
  //alert(kondisi);
  if($('#tebal').val() != "" && $('#keterangan').val() != "") {
    if(localStorage.getItem('ketebalan')){
      localStorage.setItem('ketebalan',localStorage.getItem('ketebalan') + '; ' + $('#tebal').val());
      localStorage.setItem('keterangan_tebal',localStorage.getItem('keterangan_tebal') + '; ' + $('#keterangan').val());
    } else {
      localStorage.setItem('ketebalan',$('#tebal').val());
      localStorage.setItem('keterangan_tebal',$('#keterangan').val());
    }
    alert('Data ketebalan telah masuk');
    window.location.href = "form_ketebalan_outfitting.html";
  } else {
    alert('Data belum terisi');
  }
}