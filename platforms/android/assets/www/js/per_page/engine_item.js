/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  window.location.href = "engine.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function next(id) {
  localStorage.setItem('engine_item_id',id);
  window.location.href = "engine_next.html";
}

function getList() {
  $('#nama_engine').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + localStorage.getItem('engine_nama'));
  var machinery_id = localStorage.getItem('machinery_id');
  var engine_id = localStorage.getItem('engine_id');
  var selbox = '<table class="table table-borderless">';
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/machine/engine/" + machinery_id, {} ).done( function( res ) {
    if(res.message == 'failed'){
      $('#myModal').modal('hide');
      alert('Failed to Load Data from server!');
    } else {
      $.each( res.result, function( i, item ) {
        var count = 0;
        var item_id;
        var nama;
        $.each( this, function( j, item2 ) {
          if(count == 0)
            item_id = item2;
          else if(count == 2)
            nama = item2;
          count++;
        } );
        selbox += '<tr><td colspan="3"><input type="submit" class="btn btn-lg btn-primary btn-block" value="' + nama + '" onclick="next(' + item_id + ')"></td></tr>';
      } );
    }
    selbox += '</table>';
    $('#list').html("");
    $(selbox).appendTo('#list');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    $('#myModal').modal('hide');
    alert('Failed to Load Data from server!');
  });
}