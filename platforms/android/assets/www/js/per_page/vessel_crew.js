/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button").addEventListener("click", goToSurveyList);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  }
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}

function getVesselCrew() {
  $('#myModal').modal('show');
  $.getJSON( url + "/vessel/crew", {
  } ).done( function( res ) {
    //SpinnerPlugin.activityStart('Mengunduh informasi vessel crew...');
    var str = '<table class="table table-alpha">';
    var jumlahCrew = 1;
    $.each( res.result, function(  ) {
      var count = 0;
      var id;
      var nama;
      $.each( this, function( j, item2 ) {
        if(count == 0) id = item2;
        else nama = item2;
        count++;
      } );
      if(jumlahCrew > 1 && jumlahCrew % 2 == 1) {
        str += '</tr>';
      }
      if(jumlahCrew % 2 == 1) str += '<tr>';
      str += '<td width="47%" align="center"><a href="#" role="button" class="btn btn-primary btn-block" onclick="getJob(' + id + ')">' + nama + '</a></td>';
      if(jumlahCrew % 2 == 1) str += '<td></td>';
      jumlahCrew++;
    } );
    str += '</tr></table>';
    //alert(str);
    $('#vessel_crew').html("");
    $(str).appendTo('#vessel_crew');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  } ).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function getJob(id){
  var job = 'Data Belum diinput dari web';
  $('#myModal').modal('show');
  var id_kapal = localStorage.getItem('id_kapal');
  var list_petugas = "Belum ada petugas.";
  console.log(url + "/vessel/crew/" + id + '/' + id_kapal);
  $.getJSON( url + "/vessel/crew/" + id + '/' + id_kapal, {} ).done( function( list ) {
    if(list.message == "success"){
      list_petugas = "";
      $.each( list.result, function( a ) {
        var countNama = 1;
        $.each( this, function( idx, el ) {
          list_petugas += countNama + '. ' + el + ' ';
          countNama++;
        } );
      } );
    }
  } );
  //SpinnerPlugin.activityStart("Get Data...");
  $.getJSON( url + "/vessel/crew/job/" + id, {} ).done( function( resJob ) {
    if(resJob.message == 'failed'){
      $('#myModal').modal('hide');
      alert(job);
    } else {
      // var count = 1;
      $('#headerModal').html(resJob.tipe_crew);
      job = "Nama Petugas " + ':<br><br>' + list_petugas + '<br><br>Tanggung Jawab:<br><br><ol>';
      $.each( resJob.result, function( listJob ) {
        // job += count + '. ' + this.tugas + '<br><br>';
        job += '<li>' + this.tugas + '</li>';
        // count++;
      } );
      job += '</ol>';
      //SpinnerPlugin.activityStop();
      $('#myModal').modal('hide');
      $('#contentModal').html(job);
      $('#jobModal').modal('show');
      // alert(job);
    }
  } ).fail(function () {
    $('#myModal').modal('hide');
    alert('Failed to Load Data from server!');
  });
}