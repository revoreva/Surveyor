/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button").addEventListener("click", selectHullItem);
  document.getElementById("back").addEventListener("click", back);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getHull() {
  var id = localStorage.getItem('hull_id');
  //SpinnerPlugin.activityStart("Get Data...");
  $('#nama_hull').html(localStorage.getItem('hull_nama'));
  
  var selbox = '<select class="form-control" id="hull_item_id">';
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/hull/" + id, {
  } ).done( function( res ) {
    $.each( res.result, function( i, item ) {
      var count = 0;
      var id;
      var nama;
      $.each( this, function( j, item2 ) {
        if(count == 1)
          id = item2;
        else if(count == 2)
          nama = item2;
        count++;
      } );
      selbox += '<option value="' + id + '">' + nama + '</option>';
    } );
    selbox += '</select>';
    $('#list_hull').html("");
    $(selbox).appendTo('#list_hull');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function selectHullItem() {
  var hull_item_id = $('#hull_item_id').val();
  localStorage.setItem('hull_item_id', hull_item_id);
  window.location.href = "form_hull_item.html";
}

function back() {
  window.location.href = "menu_hull.html";
}