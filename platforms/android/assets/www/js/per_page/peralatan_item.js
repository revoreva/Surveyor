/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  window.location.href = "menu_machinery.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function next(id,nama) {
  localStorage.setItem('nama_label_engine',nama);
  localStorage.setItem('engine_item_id',id);
  localStorage.setItem('last_engine','peralatan_item.html');
  window.location.href = "peralatan_next.html";
}

function getList() {
  $('#nama_engine').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + localStorage.getItem('engine_nama'));

  var machinery_id = localStorage.getItem('machinery_id');
  var selbox = '';
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  console.log(url + "/machine/engine/" + machinery_id);
  $.getJSON( url + "/machine/engine/" + machinery_id, {} ).done( function( res ) {
    $.each( res.result, function( i, item ) {
      var count = 0;
      var item_id;
      var nama;
      $.each( this, function( j, item2 ) {
        if(j == 'item_id')
          item_id = item2;
        else if(j == 'item_nama')
          nama = item2;
        count++;
      } );
      selbox += '<input type="submit" class="btn btn-lg btn-primary btn-block" value="' + nama + '" onclick="next(\'' + item_id + '\',\'' + nama + '\')">';
    } );
    $('#list').html("");
    $(selbox).appendTo('#list');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}