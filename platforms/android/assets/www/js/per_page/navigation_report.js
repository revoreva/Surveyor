/* global variables */
var url1 = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var url2 = "http://shipconditionsurvey.heliohost.org/surveykapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  window.location.href = "menu_navigations_report.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function show_image(url) {
  $("#imgModal").attr("src",url);
  // alert(url);
  // document.getElementById('imgModal').style.display = 'none';
  // document.getElementById('imgModal').style.display = 'block';
  // $('#imgModal').html("<img style=\"max-height:500px;max-width:500px;\" src=\""+ url + "\">");
  $('#gambarModal').modal('show');
}

function update(id, nama, kondisi, keterangan, gambar) {
  localStorage.setItem('navigations_id', id);
  localStorage.setItem('navigations_nama',nama);
  localStorage.setItem('report_kondisi', kondisi);
  localStorage.setItem('report_keterangan', keterangan);
  localStorage.setItem('report_gambar', gambar);
  window.location.href = "form_navigations.html";
}

function getData() {
  $('#gambarModal').on('hidden', function () {
    $("#imgModal").attr("src",'img/loading.gif');
  })
  namama = "Navigations Report";
  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);
  var id_kapal = localStorage.getItem('id_kapal');
  var id_survey = localStorage.getItem('id_survey_last');

  var selbox = '<table class="table table-alpha" style="width: 100%;"><tr>';
  selbox += '<th style="text-align:center">Item</th>';
  selbox += '<th style="text-align:center">Kondisi</th>';
  selbox += '<th style="text-align:center">Keterangan</th>';
  selbox += '<th style="text-align:center">Gambar</th>';
  selbox += '<th style="text-align:center">Update</th>';
  selbox += '</tr>';
  var count = 0;
  var navigation;
  $('#myModal').modal('show');
  $.ajax({
    type: "GET",
    url: url2 + "/report/navigation_json/" + id_kapal + '/' + id_survey,
    dataType: "text",  
    cache:false,
    success: function (response) {
      navigation = $.parseJSON(response);
      navigation = navigation.navigation;
      // console.log('success');
      // console.log(navigation);
      // console.log(url2 + "/report/document_json/" + id_kapal + '/' + id_survey);
      $('#myModal').modal('hide');
    },
    error: function (ErrorResponse) {
      alert('Failed to Load Data from server!');
      navigation = $.parseJSON('{"navigation":{"1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[],"8":[],"9":[],"10":[],"11":[],"12":[],"13":[],"14":[],"15":[],"16":[],"17":[],"18":[]}}');
      // console.log('error');
      // console.log(navigation);
      // navigation = navigation;
      $('#myModal').modal('hide');
    }
  }).done(function () {
    $.getJSON( url1 + "/navigation", {
      } ).done( function( res ) {
        // console.log(res);
        if(res.message == "success"){
          var count = 1;
          $.each( res.result, function( i, item ) {
            var countL = 0;
            var id;
            var nama;
            $.each( this, function( j, item2 ) {
              if(countL == 0)
                id = item2;
              else if(countL == 1)
                nama = item2;
              countL++;
              //alert(selbox);
            } );
            selbox += '<tr><td width="28%">' + nama + '</td>';
            // console.log(navigation[count].length);
            if(navigation[count].length > 0)
            {
              // alert('ada');
              if (navigation[count][0].navigation_data_kondisi == 'Good') {
                selbox += '<td width="18%" align="center">' + navigation[count][0].navigation_data_kondisi + '</td>';
              } else {
                selbox += '<td bgcolor="#ff4d4d" width="18%" align="center">' + navigation[count][0].navigation_data_kondisi + '</td>';
              }
              selbox += '<td width="18%" align="center">' + navigation[count][0].navigation_data_keterangan + '</td>';
              selbox += "<td width='18%' onclick=\"show_image(\'" + navigation[count][0].navigation_data_gambar + "\')\" align=\"center\"><a href=\"#\">Lihat Gambar</a></td>";

              selbox += '<td width="18%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(';
              selbox += '\'' + id + '\', ';
              selbox += '\'' + nama + '\', ';
              selbox += '\'' + navigation[count][0].navigation_data_kondisi + '\', ';
              selbox += '\'' + navigation[count][0].navigation_data_keterangan + '\', ';
              selbox += '\'' + navigation[count][0].navigation_data_gambar + '\'';
              selbox += ')"></td>';
            }
            else {
              // alert('tidak ada');
              selbox += '<td width="18%" align="center">Tidak ada data</td>';
              selbox += '<td width="18%" align="center">Tidak ada data</td>';
              selbox += '<td width="18%" align="center">Tidak ada data</td>';
              // selbox += '<td width="12%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(';
                //id, nama, kondisi, frame_start, frame_end, lajur, ketebalan, keterangan_tebal, hull_item_hull_id, keterangan, gambar
              selbox += '<td width="18%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(';
              selbox += '\'' + id + '\', ';
              selbox += '\'' + nama + '\', ';
              selbox += '\'-\', ';
              selbox += '\'-\', ';
              selbox += '\'-\'';
              selbox += ')"></td>';
            }

            selbox += '</tr>';
            // alert(selbox);
            count++;
          } );
          selbox += '</table>';
          $('#list_item').html("");
          $(selbox).appendTo('#list_item');
          // alert(selbox);
          $('#myModal').modal('hide');
          //SpinnerPlugin.activityStop();
        } else {
          alert('Failed to Load Data from server!');
          $('#myModal').modal('hide');
        }
      }).fail(function () {
        alert('Failed to Load Data from server!');
        $('#myModal').modal('hide');
      });
    });
}

function selectItem() {
  
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}