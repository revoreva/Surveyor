/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("submitHull").addEventListener("click", submitHull);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  removeStorage();
  if(localStorage.getItem('mode_report') == '1'){
    if(localStorage.getItem('last_hull') == 'superstructure')
      window.location.href = "menu_superstructure_report.html";
    else
      window.location.href = "menu_hull_report.html";
  }
  else
    window.location.href = "menu_hull_sub.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getHull() {
  if(localStorage.getItem('hull_id') == 1){
    $('#selLajur').html('<select name="lajur_select" id="value_lajur" class="form-control"><option value="K">K</option><option value="A">A</option><option value="B">B</option><option value="C">C</option><option value="D">D</option><option value="E">E</option><option value="F">F</option><option value="G">G</option></select>');
  } else {
    $('#selLajur').html('<select name="lajur_select" id="value_lajur" class="form-control"><option value="A">A</option><option value="B">B</option><option value="C">C</option><option value="D">D</option><option value="E">E</option><option value="F">F</option><option value="G">G</option></select>');
  }
  var hull_id = localStorage.getItem('hull_id');
  var hull_item_id = localStorage.getItem('hull_item_id');
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/hull/" + hull_id, {} ).done( function( res ) {
    //alert(result.result);
    if(res.message == "success"){
      $.each( res.result, function( i, item ) {
        //alert(count + ' ' + id);
        var countItem = 0;
        var ini = 0;
        $.each( this, function( j, item2 ) {
          if(countItem == 1 && item2 == hull_item_id)
            ini = 1;
          else if(countItem == 2 && ini == 1){
            $('#nama_hull_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + item2);
            namama = item2;
          }
          countItem++;
        } );
        if(ini == 1) return false;
      } );
    } else {
      bisaSubmit = 0;
      alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
    }
    //SpinnerPlugin.activityStop();
    $('#myModal').modal('hide');
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
  if (typeof localStorage.getItem('kondisi_hull_item') != 'undefined') $("input[name='kondisi'][value=\'" + localStorage.getItem('kondisi_hull_item') + "\']").prop('checked',true);
  if (typeof localStorage.getItem('frame_start_hull_item') != 'undefined') $('#frame_start').val(localStorage.getItem('frame_start_hull_item'));
  if (typeof localStorage.getItem('frame_end_hull_item') != 'undefined') $('#frame_end').val(localStorage.getItem('frame_end_hull_item'));
  if (typeof localStorage.getItem('value_lajur_hull_item') != 'undefined') $('#value_lajur').val(localStorage.getItem('value_lajur_hull_item'));
  // if (typeof localStorage.getItem('value_lajur_hull_item') != 'undefined') console.log('oi');
  if (typeof localStorage.getItem('keterangan_hull_item') != 'undefined') $('#keterangan').val(localStorage.getItem('keterangan_hull_item'));

  if(localStorage.getItem('mode_report') == '1'){
    //aa
    // $("#smallImage").attr("src",localStorage.getItem('report_gambar'));
    // document.getElementById('smallImage').style.display = 'block';
    if(localStorage.getItem('report_kondisi') == "-"){

    }
    else if(localStorage.getItem('report_kondisi') == "Good")
      $("#kondisi1").prop('checked', true);
    else
      $("#kondisi2").prop('checked', true);

    if(localStorage.getItem('frame_start') != "-"){
      $("#frame_start").val(localStorage.getItem('report_frame_start'));
    }

    if(localStorage.getItem('frame_end') != "-"){
      $("#frame_end").val(localStorage.getItem('report_frame_end'));
    }
    $('#value_lajur').val(localStorage.getItem('report_lajur'));
    // localStorage.setItem('ketebalan', localStorage.getItem('report_ketebalan'));
    // localStorage.setItem('keterangan_tebal', localStorage.getItem('report_keterangan_tebal'));
    localStorage.setItem('hull_item_id', localStorage.getItem('report_hull_item_id'));
    $('#keterangan').val(localStorage.getItem('report_keterangan'));
  }
}

function bantuan() {
  var bantuan = 'Data Belum diinput dari web';
  var id1 = localStorage.getItem('hull_id');
  var id2 = localStorage.getItem('hull_item_id');
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/hull/bantuan/" + id1 + '/' + id2, {} ).done( function( resJob ) {
    if(resJob.message == 'failed'){
      $('#myModal').modal('hide');
      alert(bantuan);
    } else {
      // var count = 1;
      bantuan = "<ol>";
      $.each( resJob.result, function( listJob ) {
        // bantuan += count + '. ' + this.bantuan + '\n\n';
        bantuan += '<li>' + this.bantuan + '</li>';
        // count++;
      } );
    }
    bantuan += '</ol>';
    $('#contentModal').html(bantuan);
    $('#bantuanModal').modal('show');
    //SpinnerPlugin.activityStop();
    $('#myModal').modal('hide');
    // alert(bantuan);
  } ).fail(function () {
    $('#myModal').modal('hide');
    alert('Failed to Load Data from server!');
  });
}

function inputKetebalan() {
  insertStorage();
  window.location.href = "form_ketebalan.html";
}

function insertStorage() {
  localStorage.setItem('kondisi_hull_item',$("input[name='kondisi']:checked").val());
  localStorage.setItem('frame_start_hull_item',$('#frame_start').val());
  localStorage.setItem('frame_end_hull_item',$('#frame_end').val());
  localStorage.setItem('value_lajur_hull_item',$('#value_lajur').val());
  localStorage.setItem('keterangan_hull_item',$('#keterangan').val());
}

function removeStorage() {
  localStorage.removeItem('kondisi_hull_item');
  localStorage.removeItem('frame_start_hull_item');
  localStorage.removeItem('frame_end_hull_item');
  localStorage.removeItem('value_lajur_hull_item');
  localStorage.removeItem('keterangan_hull_item');
  localStorage.removeItem('keterangan_tebal');
  localStorage.removeItem('ketebalan');
}

function submitHull(argument) {
  if(bisaSubmit){
    var kondisi = $("input[name='kondisi']:checked").val();
    var gambar = "";
    gambar = document.getElementById('smallImage').getAttribute("src");
    if($('#frame_start').val() != "" && $('#frame_end').val() != "" && $('#value_lajur').val() != "" && $('#keterangan').val() != "") {
      var dataToBeSent = {
        'kondisi' : kondisi,
        'start' : $('#frame_start').val(),
        'end' : $('#frame_end').val(),
        'lajur' : $('#value_lajur').val(),
        'keterangan' : $('#keterangan').val(),
        'hull_item_id' : localStorage.getItem('hull_item_id'),
        'vessel_imo' : localStorage.getItem('id_kapal'),
        'survey' : localStorage.getItem('id_survey'),
        'hull_id': localStorage.getItem('hull_id'),
        'gambar' : gambar,
        'keterangan_tebal' : localStorage.getItem('keterangan_tebal'),
        'ketebalan' : localStorage.getItem('ketebalan')
      };
      SpinnerPlugin.activityStart("Submitting Data...");
      $.post(url + "/hull", dataToBeSent, function(data, textStatus) {
        if(data.message == 'failed'){
          alert('Ada kesalahan data / jaringan!');
        } else {
          alert('data pengecekan berhasil terisi');
          removeStorage();
          window.location.href = "form_hull_item.html";
        }
        SpinnerPlugin.activityStop();
      }, "json");
    } else {
      alert('Data harus terisi semua!');
    }
  } else {
    alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
  }
}