/* global variables */
var url1 = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var url2 = "http://shipconditionsurvey.heliohost.org/surveykapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  window.location.href = "cor_report.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function show_image(url) {
  $("#imgModal").attr("src",url);
  // alert(url);
  // document.getElementById('imgModal').style.display = 'none';
  // document.getElementById('imgModal').style.display = 'block';
  // $('#imgModal').html("<img style=\"max-height:500px;max-width:500px;\" src=\""+ url + "\">");
  $('#gambarModal').modal('show');
}

function update(id, nama, ketersediaan, status, tanggal_start, tanggal_finish, gambar) {
  localStorage.setItem('id_certificate', id);
  localStorage.setItem('name_certificate',nama);
  localStorage.setItem('report_ketersediaan', ketersediaan);
  localStorage.setItem('report_status', status);
  localStorage.setItem('report_tanggal_start', tanggal_start);
  localStorage.setItem('report_tanggal_finish', tanggal_finish);
  localStorage.setItem('report_gambar', gambar);
  window.location.href = "certificate.html";
}

function getData() {
  $('#gambarModal').on('hidden', function () {
    $("#imgModal").attr("src",'img/loading.gif');
  })
  namama = "Certificate Report";
  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);
  var id_kapal = localStorage.getItem('id_kapal');
  var id_survey = localStorage.getItem('id_survey_last');

  var selbox = '<table class="table table-alpha" style="width: 100%;"><tr><th style="text-align:center">Jenis Sertifikat</th><th style="text-align:center">Ketersediaan</th><th style="text-align:center">Status</th><th style="text-align:center">Tanggal Berlaku</th><th style="text-align:center">Gambar</th><th style="text-align:center">Update</th></tr>';
  var count = 0;
  var sertifikat;
  $('#myModal').modal('show');
  $.ajax({
    type: "GET",
    url: url2 + "/report/certificate_json/" + id_kapal + '/' + id_survey,
    dataType: "text",  
    cache:false,
    success: function (response) {
      sertifikat = $.parseJSON(response);
      sertifikat = sertifikat.sertifikat;
      // console.log('success');
      // console.log(sertifikat);
      // console.log(url2 + "/report/certificate_json/" + id_kapal + '/' + id_survey);
      $('#myModal').modal('hide');
    },
    error: function (ErrorResponse) {
      alert('Failed to Load Data from server!');
      sertifikat = $.parseJSON('{"sertifikat":{"1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[],"8":[],"9":[],"10":[],"11":[],"12":[]}}');
      // console.log('error');
      // console.log(sertifikat);
      // sertifikat = sertifikat;
      $('#myModal').modal('hide');
    }
  }).done(function () {
    $.getJSON( url1 + "/certificates", {
      } ).done( function( res ) {
        //SpinnerPlugin.activityStart("Mengunduh Data List Certificate & Document...");
        // selbox += '<option value="0" disabled selected>Select The Item Below..</option>';
        // console.log('res');
        // console.log(res);
        if(res.message == "success"){
          var count = 1;
          $.each( res.result, function( i, item ) {
            var countL = 0;
            var id;
            var nama;
            $.each( this, function( j, item2 ) {
              if(countL == 0)
                id = item2;
              else 
                nama = item2;
              countL++;
              //alert(selbox);
            } );
            selbox += '<tr><td width="25%">' + nama + '</td>';
            // console.log(sertifikat[count].length);
            if(sertifikat[count].length > 0)
            {
              // alert('ada');
              if(sertifikat[count][0].cer_content_ada != "Ada")
                selbox += "<td bgcolor='#ff4d4d' align='center' width='15%'>" + sertifikat[count][0].cer_content_ada + "</td>";
              else 
                selbox += "<td align='center' width='15%'>" + sertifikat[count][0].cer_content_ada + "</td>";

              if(sertifikat[count][0].cer_content_status == "Kadaluarsa")
                selbox += "<td bgcolor='#ff4d4d' align='center' width='15%'>" + sertifikat[count][0].cer_content_status + "</td>";
              else 
                selbox += "<td align='center' width='15%'>" + sertifikat[count][0].cer_content_status + "</td>";

              selbox += "<td align=\"center\" width='15%'>" + sertifikat[count][0].cer_content_date_start + " s.d " + sertifikat[count][0].cer_content_date_finish + "</td>";

              selbox += "<td width='15%' onclick=\"show_image(\'" + sertifikat[count][0].cer_gambar_path + "\')\" align=\"center\"><a href=\"#\">Lihat Gambar</a></td>";

              selbox += '<td width="15%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(\'' + id + '\', \'' + nama + '\', \'' + sertifikat[count][0].cer_content_ada + '\', \'' + sertifikat[count][0].cer_content_status + '\', \'' + sertifikat[count][0].cer_content_date_start + '\', \'' + sertifikat[count][0].cer_content_date_finish + '\', \'' + sertifikat[count][0].cer_gambar_path + '\')"></td>';                
            }
            else {
              // alert('tidak ada');
              selbox += '<td width="15%" align="center">Tidak ada data</td><td align="center" width="15%">Tidak ada data</td><td align="center" width="15%">Tidak ada data</td><td align="center" width="15%">Tidak ada data</td>';
              selbox += '<td width="15%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(\'' + id + '\', \'' + nama + '\', \'-\', \'-\', \'-\', \'-\', \'-\')"></td>';
            }

            selbox += '</tr>';
            // alert(selbox);
            count++;
          } );
          selbox += '</table>';
          $('#list_item').html("");
          $(selbox).appendTo('#list_item');
          // alert(selbox);
          $('#myModal').modal('hide');
          //SpinnerPlugin.activityStop();
        } else {
          alert('Failed to Load Data from server!');
          $('#myModal').modal('hide');
        }
      }).fail(function () {
        alert('Failed to Load Data from server!');
        $('#myModal').modal('hide');
      });
      // alert('nanana' + selbox);
    });
  /*
  $.getJSON( url2 + "/report/certificate_json/" + id_kapal + '/' + id_survey, {} ).done( function( res ) {
    sertifikat = $.parseJSON(res);
    $('#myModal').modal('hide');
  }).fail(function () {
    alert('Failed to Load Data from server!');
    sertifikat = $.parseJSON('{"sertifikat":{"1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[],"8":[],"9":[],"10":[],"11":[],"12":[]}}'); 
    $('#myModal').modal('hide');
  });
  */  
}

function selectItem() {
  
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}