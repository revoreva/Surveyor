/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("back").addEventListener("click", back);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  //localStorage.removeItem('item');
  localStorage.clear();
  window.location.href = "index.html";
}

function getData(){
  var selbox = "";
  $('#myModal').modal('show');
  $.getJSON( url + "/hull/superstructure", {
  } ).done( function( res ) {
    if(res.message == 'success'){
      $.each( res.result, function( i, item ) {
        var id;
        var nama;
        $.each( this, function( j, item2 ) {
          if(j == 'hull_id')
            id = item2;
          else if(j == 'hull_nama')
            nama = item2;
        } );
        selbox += '<input type="submit" class="btn btn-lg btn-primary btn-block" value="' + nama + '" onclick="next(\'' + id + '\',\'' + nama + '\')">';
      } );
    } else {
      //alert("")
      $('#myModal').modal('hide');
    }
    $('#list').html(selbox);
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function next(id,nama) {
  localStorage.setItem('hull_id',id);
  localStorage.setItem('hull_nama',nama);
  window.location.href = "menu_hull_sub.html";
}

function back() {
  window.location.href = "pilihan_hull.html";
}