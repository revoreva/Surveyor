/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button").addEventListener("click", goToFormCertificate);
  document.getElementById("back").addEventListener("click", back);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
  getCertificate();
  getGambar();
}

function back() {
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "certificate_report.html";
  else
    window.location.href = "cor.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getCertificate() {
  var name = localStorage.getItem('name_certificate');
  if(name.length > 10){
    name = name.substring(0,10) + '..';
  }
  $('#name_cert').html('<button id="back" type="button" class="btn btn-default"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + name);
}

function getGambar() {
  var id1 = localStorage.getItem('id_certificate');
  var id2 = localStorage.getItem('id_kapal');
  //SpinnerPlugin.activityStart("Getting Picture...");
  $('#myModal').modal('show');
  console.log(url + "/certificate/" + id1 + '/' + id2);
  $.getJSON( url + "/certificate/" + id1 + '/' + id2, {
  } ).done( function( res ) {
    if(res.message == "success"){
      $.each( res.result, function() {
        var src = "";
        $.each( this, function( j, item2 ) {
          var gambar = document.getElementById('gambar');
            src += item2;
        } );
        gambar.src = src;
        $('#myModal').modal('hide');
        //SpinnerPlugin.activityStop();
      } );
    } else {
      alert('Data tidak ada');
      $('#myModal').modal('hide');
    }
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function goToFormCertificate() {
  window.location.href = "formCertificate.html";
}