/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("back").addEventListener("click", back);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
  getDocument();
  getGambar();
}

function back() {
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "document_report.html";
  else
    window.location.href = "cor.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getDocument() {
  var name = localStorage.getItem('name_document');
  if(name.length > 10){
    name = name.substring(0,10) + '..';
  }
  $('#name_doc').html('<button id="back" type="button" class="btn btn-default"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + name);
}

function getGambar() {
  
  var id1 = localStorage.getItem('id_document');
  var id2 = localStorage.getItem('id_kapal');
  $('#myModal').modal('show');
  $.getJSON( url + "/certificate/" + id1 + '/' + id2, {
  } ).done( function( res ) {
    if(res.message == "success"){
      $.each( res.result, function() {
        var src = "";
        $.each( this, function( j, item2 ) {
          var gambar = document.getElementById('gambar');
            src += item2;
        } );
        gambar.src = src;
        $('#myModal').modal('hide');
      } );
    } else {
      alert('Data tidak ada');
      $('#myModal').modal('hide');
    }
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function submitDocument() {
  var kondisi = "-";
  var status = "-";
  var date_start = "-";
  var date_end = "-";
  var gambar = "";
  gambar = document.getElementById('smallImage').getAttribute("src");
  if(gambar != "") {
    var dataToBeSent = {
      'ada' : '-',
      'status' : '-',
      'start_date' : localStorage.getItem('date'),
      'end_date' : localStorage.getItem('date'),
      'survey_date' : localStorage.getItem('date'),
      'vessel_imo' : localStorage.getItem('id_kapal'),
      'cer_id' : localStorage.getItem('id_document'),
      'id_survey' : localStorage.getItem('id_survey'),
      'gambar' : gambar
    };
    SpinnerPlugin.activityStart("Submitting Data...");
    $.post(url + "/certificate", dataToBeSent, function(data, textStatus) {
      if(data.message == 'failed'){
        alert('Ada kesalahan data / jaringan!');
      } else {
        alert('data pengecekan berhasil terisi');
        window.location.href = "document.html";
      }
      SpinnerPlugin.activityStop();
    }, "json");
  } else {
    alert('Data gambar harus terisi!');
  }
}