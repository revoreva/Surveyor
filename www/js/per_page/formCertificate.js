/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("submitCertificate").addEventListener("click", submitCertificate);
  document.getElementById("back").addEventListener("click", back);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  //localStorage.removeItem('item');
  localStorage.clear();
  window.location.href = "index.html";
}

function getCertificate() {
  var name = localStorage.getItem('name_certificate');
  $('#name_cert').html(name + ' :');
}

function generateDate() {
  var today = new Date();

  var tglNow = today.getDate();
  var selbox = '<select class="sel form-control" id="tgl_start_certificate">';
  for(var tgl = 1; tgl <= 30; tgl++) selbox += '<option value="' + tgl + '" ' + (tgl == tglNow ? ' selected' : '') + '>' + tgl + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyStart');

  var blnNow = today.getMonth()+1;
  selbox = '<select class="sel form-control" id="bln_start_certificate">';
  var bulan = ['-','Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
  for(var bln = 1; bln <= 12; bln++) selbox += '<option value="' + bln + '" ' + (bln == blnNow ? ' selected' : '') + '>' + bulan[bln] + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyStart');

  var thnNow = today.getFullYear();
  selbox = '<select class="sel form-control" id="thn_start_certificate">';
  for(var thn = thnNow; thn >= 2000; thn--) selbox += '<option value="' + thn + '">' + thn + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyStart');

  selbox = '<select class="sel form-control" id="tgl_end_certificate">';
  for(var tgl = 1; tgl <= 30; tgl++) selbox += '<option value="' + tgl + '" ' + (tgl == tglNow ? ' selected' : '') + '>' + tgl + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyEnd');

  selbox = '<select class="sel form-control" id="bln_end_certificate">';
  var bulan = ['-','Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
  for(var bln = 1; bln <= 12; bln++) selbox += '<option value="' + bln + '" ' + (bln == blnNow ? ' selected' : '') + '>' + bulan[bln] + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyEnd');

  selbox = '<select class="sel form-control" id="thn_end_certificate">';
  for(var thn = thnNow; thn >= 2000; thn--) selbox += '<option value="' + thn + '">' + thn + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyEnd');

  if(localStorage.getItem('mode_report') == '1'){
    //aa
    // $("#smallImage").attr("src",localStorage.getItem('report_gambar'));
    // document.getElementById('smallImage').style.display = 'block';
    if(localStorage.getItem('report_ketersediaan') == "Ada")
      $("#ketersediaan1").prop('checked', true);
    else
      $("#ketersediaan2").prop('checked', true);

    if(localStorage.getItem('report_status') == "Berlaku")
      $("#status1").prop('checked', true);
    else
      $("#status2").prop('checked', true);

    var report_tgl_start = localStorage.getItem('report_tanggal_start').split('-');
    var report_tgl_finish = localStorage.getItem('report_tanggal_finish').split('-');
    // console.log(Number(report_tgl_start[1]));
    console.log(bulan[Number(report_tgl_start[1])]);
    $('#tgl_start_certificate').val(report_tgl_start[2]);
    $('#bln_start_certificate').val(Number(report_tgl_start[1]));
    $('#thn_start_certificate').val(report_tgl_start[0]);

    $('#tgl_end_certificate').val(report_tgl_finish[2]);
    $('#bln_end_certificate').val(Number(report_tgl_finish[1]));
    $('#thn_end_certificate').val(report_tgl_finish[0]);

  }
}

function submitCertificate() {
  var kondisi = "";
  kondisi = $("input[name='adaCertificate']:checked").val();
  var status = "";
  status = $("input[name='statusCertificate']:checked").val();
  var date_start = "";
  date_start = $('#bln_start_certificate').val() + '/' + $('#tgl_start_certificate').val() + '/' + $('#thn_start_certificate').val();
  var date_end = "";
  date_end = $('#bln_end_certificate').val() + '/' + $('#tgl_end_certificate').val() + '/' + $('#thn_end_certificate').val(); 
  var gambar = "";
  gambar = document.getElementById('smallImage').getAttribute("src");

  if(kondisi != "" && status != "" && date_start != "" && date_end != "") {
    var dataToBeSent = {
      'ada' : kondisi,
      'status' : status,
      'start_date' : date_start,
      'end_date' : date_end,
      'survey_date' : localStorage.getItem('date'),
      'vessel_imo' : localStorage.getItem('id_kapal'),
      'cer_id' : localStorage.getItem('id_certificate'),
      'id_survey' : localStorage.getItem('id_survey'),
      'gambar' : gambar
    };
    SpinnerPlugin.activityStart("Submitting Data...");
    $.post(url + "/certificate", dataToBeSent, function(data, textStatus) {
      if(data.message == 'failed'){
        alert('Ada kesalahan data / jaringan!');
      } else {
        alert('data pengecekan berhasil terisi');
        window.location.href = "formCertificate.html";
      }
      SpinnerPlugin.activityStop();
    }, "json");
  } else {
    alert('Data harus terisi semua!');
  }
}

function back() {
  window.location.href = "certificate.html";
}