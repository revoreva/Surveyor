/* global variables */
var url1 = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var url2 = "http://shipconditionsurvey.heliohost.org/surveykapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  window.location.href = "cor_report.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function show_image(url) {
  $("#imgModal").attr("src",url);
  // alert(url);
  // document.getElementById('imgModal').style.display = 'none';
  // document.getElementById('imgModal').style.display = 'block';
  // $('#imgModal').html("<img style=\"max-height:500px;max-width:500px;\" src=\""+ url + "\">");
  $('#gambarModal').modal('show');
}

function update(id, nama, gambar) {
  localStorage.setItem('id_document', id);
  localStorage.setItem('name_document',nama);
  localStorage.setItem('report_gambar', gambar);
  window.location.href = "document.html";
}

function getData() {
  $('#gambarModal').on('hidden', function () {
    $("#imgModal").attr("src",'img/loading.gif');
  })
  namama = "Document Report";
  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);
  var id_kapal = localStorage.getItem('id_kapal');
  var id_survey = localStorage.getItem('id_survey_last');

  var selbox = '<table class="table table-alpha" style="width: 100%;"><tr><th style="text-align:center">Jenis Dokumen</th><th style="text-align:center">Gambar</th><th style="text-align:center">Update</th></tr>';
  var count = 0;
  var dokumen;
  $('#myModal').modal('show');
  $.ajax({
    type: "GET",
    url: url2 + "/report/document_json/" + id_kapal + '/' + id_survey,
    dataType: "text",  
    cache:false,
    success: function (response) {
      dokumen = $.parseJSON(response);
      dokumen = dokumen.dokumen;
      console.log('success');
      console.log(dokumen);
      console.log(url2 + "/report/document_json/" + id_kapal + '/' + id_survey);
      $('#myModal').modal('hide');
    },
    error: function (ErrorResponse) {
      alert('Failed to Load Data from server!');
      dokumen = $.parseJSON('{"dokumen":{"12":[],"13":[],"14":[],"15":[],"16":[],"17":[]}}');
      console.log('error');
      console.log(dokumen);
      // dokumen = dokumen;
      $('#myModal').modal('hide');
    }
  }).done(function () {
    $.getJSON( url1 + "/documents", {
      } ).done( function( res ) {
        //SpinnerPlugin.activityStart("Mengunduh Data List Document & Document...");
        // selbox += '<option value="0" disabled selected>Select The Item Below..</option>';
        // console.log('res');
        // console.log(res);
        if(res.message == "success"){
          var count = 12;
          $.each( res.result, function( i, item ) {
            var countL = 0;
            var id;
            var nama;
            $.each( this, function( j, item2 ) {
              if(countL == 0)
                id = item2;
              else 
                nama = item2;
              countL++;
              //alert(selbox);
            } );
            selbox += '<tr><td width="40%">' + nama + '</td>';
            // console.log(dokumen[count].length);
            if(dokumen[count].length > 0)
            {
              // alert('ada');
              selbox += "<td width='30%' onclick=\"show_image(\'" + dokumen[count][0].cer_gambar_path + "\')\" align=\"center\"><a href=\"#\">Lihat Gambar</a></td>";

              selbox += '<td width="30%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(\'' + id + '\', \'' + nama + '\', \'' + dokumen[count][0].cer_gambar_path + '\')"></td>';
                
            }
            else {
              // alert('tidak ada');
              selbox += '<td width="30%" align="center">Tidak ada data</td>';
              selbox += '<td width="30%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(\'' + id + '\', \'' + nama + '\', \'-\')"></td>';
            }

            selbox += '</tr>';
            // alert(selbox);
            count++;
          } );
          selbox += '</table>';
          $('#list_item').html("");
          $(selbox).appendTo('#list_item');
          // alert(selbox);
          $('#myModal').modal('hide');
          //SpinnerPlugin.activityStop();
        } else {
          alert('Failed to Load Data from server!');
          $('#myModal').modal('hide');
        }
      }).fail(function () {
        alert('Failed to Load Data from server!');
        $('#myModal').modal('hide');
      });
      // alert('nanana' + selbox);
    });
  /*
  $.getJSON( url2 + "/report/document_json/" + id_kapal + '/' + id_survey, {} ).done( function( res ) {
    dokumen = $.parseJSON(res);
    $('#myModal').modal('hide');
  }).fail(function () {
    alert('Failed to Load Data from server!');
    dokumen = $.parseJSON('{"dokumen":{"1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[],"8":[],"9":[],"10":[],"11":[],"12":[]}}'); 
    $('#myModal').modal('hide');
  });
  */  
}

function selectItem() {
  
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}