/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button").addEventListener("click", selectItem);
  document.getElementById("back").addEventListener("click", back);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getData() {
  var selbox = '<select class="form-control" id="item_id">';
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/outfitting", {
  } ).done( function( res ) {
    //alert(result.result);
    $.each( res.result, function( i, item ) {
      var count = 1;
      var id;
      var nama;
      $.each( this, function( j, item2 ) {
        if(count == 1)
          id = item2;
        else if(count == 2)
          nama = item2;
        count++;
      } );
      selbox += '<option value="' + id + '">' + nama + '</option>';
    } );
    selbox += '</select>';
    $('#list_item').html("");
    $(selbox).appendTo('#list_item');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function selectItem() {
  var id = $('#item_id').val();
  var nama = $("#item_id option[value='" + id + "']").text();
  localStorage.setItem('outfitting_id', id);
  localStorage.setItem('outfitting_nama',nama);
  window.location.href = "form_outfitting.html";
}

function back() {
  window.location.href = "survey_list.html";
}