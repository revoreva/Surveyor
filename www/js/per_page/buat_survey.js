/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button").addEventListener("click", submitSurvey);
  document.getElementById("buttonLanjut").addEventListener("click", lanjutSurvey);
} );

function lanjutSurvey() {
  localStorage.setItem('mode_report','0');
  window.location.href = "list_survey.html";
}

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  }
  // if(localStorage.getItem('role') == 'admin'){
  //   alert('you must logged in as user');
  //   window.location.href = "index.html";
  // }
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function submitSurvey() {
  localStorage.setItem('mode_report','1');
  localStorage.setItem('date',$('#bln_survey').val() + '/' + $('#tgl_survey').val() + '/' + $('#thn_survey').val());
  localStorage.setItem('company',$('#company').val());
  localStorage.setItem('surveyor',$('#surveyor').val());


  var dataToBeSent = {
      'date' : localStorage.getItem('date'),
      'company' : localStorage.getItem('company'),
      'surveyor' : localStorage.getItem('surveyor'),
      'username' : localStorage.getItem('username'),
      'vessel_imo' : localStorage.getItem('id_kapal')
  };
  SpinnerPlugin.activityStart("Submitting Data...");
  $.post(url + "/survey", dataToBeSent, function(data, textStatus) {
      if(data.message == 'failed'){
        alert('Ada kesalahan pengiriman data');
      } else {
        alert('Data survey berhasil dimasukkan');
        localStorage.setItem('id_survey',data.id_survey);
        window.location.href = "info_kapal.html";
      }
      SpinnerPlugin.activityStop();
    }, "json");
}

function generateDate() {
  var today = new Date();

  var tglNow = today.getDate();
  var selbox = '<select class="form-control sel" id="tgl_survey">';
  for(var tgl = 1; tgl <= 30; tgl++) selbox += '<option value="' + tgl + '" ' + (tgl == tglNow ? ' selected' : '') + '>' + tgl + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurvey');

  var blnNow = today.getMonth()+1;
  selbox = '<select class="form-control sel" id="bln_survey">';
  var bulan = ['-','Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
  for(var bln = 1; bln <= 12; bln++) selbox += '<option value="' + bln + '" ' + (bln == blnNow ? ' selected' : '') + '>' + bulan[bln] + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurvey');

  var thnNow = today.getFullYear();
  selbox = '<select class="form-control sel" id="thn_survey">';
  for(var thn = thnNow; thn >= 2000; thn--) selbox += '<option value="' + thn + '">' + thn + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurvey');
}