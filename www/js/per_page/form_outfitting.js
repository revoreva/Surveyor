/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();

} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  removeStorage();
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "menu_outfitting_report.html";
  else
    window.location.href = "menu_outfitting.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getData() {
  var outfitting_id = localStorage.getItem('outfitting_id');
  var id_kapal = localStorage.getItem('id_kapal');
  namama = localStorage.getItem('outfitting_nama');
  if(namama.length > 17){
    namama = namama.substring(0,17) + '..';
  }

  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);

  if(outfitting_id != 2 && (outfitting_id < 7 || outfitting_id > 9) ){
    var element = document.getElementById("petunjukInput");
    element.parentNode.removeChild(element);
    var selbox = '<table class="table table-alpha">';
    element = document.getElementById("buttonKetebalan");
    element.parentNode.removeChild(element);
    element = document.getElementById("frameOutfitting");
    element.parentNode.removeChild(element);
    element = document.getElementById("labelFrame");
    element.parentNode.removeChild(element);
    //SpinnerPlugin.activityStart("Get Data...");
    $('#myModal').modal('show');
    console.log(url + "/outfitting/" + outfitting_id + '/' + id_kapal);
    $.getJSON( url + "/outfitting/" + outfitting_id + '/' + id_kapal, {} ).done( function( res ) {
      if(res.message == "success"){
        $.each( res.result, function( j, item2 ) {
          if(j != 'item_id' && j != 'vessel_imo' && j != 'outfitting_id'){
            selbox += '<tr><td width="50%">' + j + '</td><td width="50%">' + item2 + '</td></tr>';
          } else if(j == 'outfitting_id'){
            localStorage.setItem('outfitting_id_submit',item2);
          }
        } );
        selbox += '</table>';
        $('#list_item').html(selbox);
      } else {
        bisaSubmit = 0;
        alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
      }
      $('#myModal').modal('hide');
      //SpinnerPlugin.activityStop();
    }).fail(function () {
      alert('Failed to Load Data from server!');
      $('#myModal').modal('hide');
    });
  } else {
    //$('#list_item').html("");
    if(outfitting_id != 8){
      element = document.getElementById("frameOutfitting");
      element.parentNode.removeChild(element);
      element = document.getElementById("labelFrame");
      element.parentNode.removeChild(element);
    }
    var element = document.getElementById("contentInfo");
    element.parentNode.removeChild(element);
  }
  $('#myModal').modal('show');
  console.log(url + "/outfitting/" + outfitting_id + '/' + id_kapal);
  $.getJSON( url + "/outfitting/" + outfitting_id + '/' + id_kapal, {} ).done( function( res ) {
    if(res.message == "success"){
      $.each( res.result, function( j, item2 ) {
        if(j == 'outfitting_id'){
          localStorage.setItem('outfitting_id_submit',item2);
        }
      } );
    } else {
      bisaSubmit = 0;
      alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
    }
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });

  if (typeof localStorage.getItem('kondisi_outfitting_item') != 'undefined'){
    $("input[name='kondisi'][value=\'" + localStorage.getItem('kondisi_outfitting_item') + "\']").prop('checked',true);
  }
  if(outfitting_id == 8){
    if (typeof localStorage.getItem('frame_start_outfitting_item') != 'undefined') $('#frame_start').val(localStorage.getItem('frame_start_outfitting_item'));
    if (typeof localStorage.getItem('frame_end_outfitting_item') != 'undefined') $('#frame_end').val(localStorage.getItem('frame_end_outfitting_item'));
  }
  if (typeof localStorage.getItem('keterangan_outfitting_item') != 'undefined') $('#keterangan').val(localStorage.getItem('keterangan_outfitting_item'));
  
  if(localStorage.getItem('mode_report') == '1'){
    //aa
    // $("#smallImage").attr("src",localStorage.getItem('report_gambar'));
    // document.getElementById('smallImage').style.display = 'block';
    if(localStorage.getItem('report_kondisi') == "-"){

    }
    else if(localStorage.getItem('report_kondisi') == "Good")
      $("#kondisi1").prop('checked', true);
    else
      $("#kondisi2").prop('checked', true);

    if(localStorage.getItem('frame_start') != "-"){
      $("#frame_start").val(localStorage.getItem('report_frame_start'));
    }

    if(localStorage.getItem('frame_end') != "-"){
      $("#frame_end").val(localStorage.getItem('report_frame_end'));
    }
    // localStorage.setItem('keterangan_tebal', localStorage.getItem('report_keterangan_tebal'));
    // localStorage.setItem('ketebalan', localStorage.getItem('report_ketebalan'));
    $('#keterangan').val(localStorage.getItem('report_keterangan'));
    localStorage.setItem('outfitting_id_submit', localStorage.getItem('report_outfitting_submit'));
  }
}

function selectItem() {
  if(bisaSubmit){
    var kondisi = "";
    kondisi = $("input[name='kondisi']:checked").val();
    var frame_start = 0;
    var frame_end = 0;
    if(localStorage.getItem('outfitting_id') == 8){
      frame_start = $('#frame_start').val();
      frame_end = $('#frame_end').val();
    }
    var ketebalan = "0";
    var keterangan_tebal = "-";
    var outfitting_id = localStorage.getItem('outfitting_id');
    if(outfitting_id == 2 || (outfitting_id >= 7 && outfitting_id <= 9 )){
      ketebalan = localStorage.getItem('ketebalan');
      keterangan_tebal = localStorage.getItem('keterangan_tebal');
    }
    var gambar = document.getElementById('smallImage').getAttribute("src");
    if(kondisi != "") {
      var dataToBeSent = {
        'kondisi' : kondisi,
        'frame_start' : frame_start,
        'frame_end' : frame_end,
        'gambar' : gambar,
        'keterangan' : $('#keterangan').val(),
        'id_survey' : localStorage.getItem('id_survey'),
        'vessel_imo' : localStorage.getItem('id_kapal'),
        'outfitting_item_id' : outfitting_id,
        'keterangan_tebal' : keterangan_tebal,
        'ketebalan' : ketebalan,
        // 'outfitting_id' : localStorage.getItem('outfitting_id')
        'outfitting_id' : localStorage.getItem('outfitting_id_submit')
      };
      SpinnerPlugin.activityStart("Submitting Data...");
      $.post(url + "/outfitting", dataToBeSent, function(data, textStatus) {
        if(data.message == 'failed'){
          alert('Ada kesalahan data / jaringan!');
        } else {
          alert('data pengecekan berhasil terisi');
          removeStorage();
          window.location.href = "form_outfitting.html";
        }
        SpinnerPlugin.activityStop();
      }, "json");
    } else {
      alert('Data harus terisi semua!');
    }
  } else {
    alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
  }
}

function inputKetebalan() {
  insertStorage();
  window.location.href = "form_ketebalan_outfitting.html";
}

function insertStorage() {
  localStorage.setItem('kondisi_outfitting_item',$("input[name='kondisi']:checked").val());
  localStorage.setItem('frame_start_outfitting_item',$('#frame_start').val());
  localStorage.setItem('frame_end_outfitting_item',$('#frame_end').val());
  localStorage.setItem('keterangan_outfitting_item',$('#keterangan').val());
}

function removeStorage() {
  localStorage.removeItem('kondisi_outfitting_item');
  localStorage.removeItem('frame_start_outfitting_item');
  localStorage.removeItem('frame_end_outfitting_item');
  localStorage.removeItem('keterangan_outfitting_item');
  localStorage.removeItem('keterangan_tebal');
  localStorage.removeItem('ketebalan');
}

function bantuan() {
  var bantuan = 'Data Belum diinput dari web';
  var id1 = localStorage.getItem('outfitting_id');
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/outfitting/bantuan/" + id1, {} ).done( function( resJob ) {
    if(resJob.message == 'failed'){
      $('#myModal').modal('hide');
      alert(bantuan);
    } else {
      // var count = 1;
      bantuan = "<ol>";
      $.each( resJob.result, function( listJob ) {
        // bantuan += count + '. ' + this.bantuan + '\n\n';
        bantuan += '<li>' + this.bantuan + '</li>';
        // count++;
      } );
    }
    bantuan += '</ol>';
    $('#contentModal').html(bantuan);
    $('#bantuanModal').modal('show');
    //SpinnerPlugin.activityStop();
    $('#myModal').modal('hide');
    // alert(bantuan);
  } ).fail(function () {
    $('#myModal').modal('hide');
    alert('Failed to Load Data from server!');
  });
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}