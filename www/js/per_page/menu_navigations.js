/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("back").addEventListener("click", back);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getNav() {
  var selbox = '<select class="form-control" id="item_nav" onchange="goToNav(this)">';
  selbox += '<option value="-" disabled selected>Select The Item Below..</option>';
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/navigation", {
  } ).done( function( res ) {
    $.each( res.result, function( i, item ) {
      var count = 1;
      var id;
      var nama;
      $.each( this, function( j, item2 ) {
        if(count == 1)
          id = item2;
        else if(count == 2)
          nama = item2;
        count++;
        //alert(selbox);
      } );
      selbox += '<option value="' + id + '">' + nama + '</option>';
    } );
    selbox += '</select>';
    $('#list_nav').html("");
    $(selbox).appendTo('#list_nav');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function getSaf() {
  var selbox = '<select class="form-control" id="item_saf" onchange="goToSaf(this)">';
  selbox += '<option value="-" disabled selected>Select The Item Below..</option>';
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/safety", {
  } ).done( function( res ) {
    $.each( res.result, function( i, item ) {
      var count = 1;
      var id;
      var nama;
     
      $.each( this, function( j, item2 ) {
        if(count == 1)
          id = item2;
        else if(count == 2)
          nama = item2;
        count++;
      } );
      selbox += '<option value="' + id + '">' + nama + '</option>';
    } );
    selbox += '</select>';
    $('#list_saf').html("");
    $(selbox).appendTo('#list_saf');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function goToNav(element) {
  var id = element.value;
  var nama = element.options[element.selectedIndex].text;
  localStorage.setItem('navigations_id', id);
  localStorage.setItem('navigations_nama',nama);
  window.location.href = "form_navigations.html";
}

function goToSaf(element) {
  var id = element.value;
  var nama = element.options[element.selectedIndex].text;
  localStorage.setItem('safety_id', id);
  localStorage.setItem('safety_nama',nama);
  window.location.href = "form_safety.html";
}

function back() {
  window.location.href = "survey_list.html";
}