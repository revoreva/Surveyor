/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  //alert(localStorage.getItem('last_engine'));
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "menu_machinery_report.html";
  else
    window.location.href = localStorage.getItem('last_engine');
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getData() {
  namama = localStorage.getItem('nama_label_engine');
  if(namama.length > 17){
    namama = namama.substring(0,17) + '..';
  }
  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);
  var machinery_id = localStorage.getItem('machinery_id');
  var engine_id = localStorage.getItem('engine_id');
  var engine_item_id = localStorage.getItem('engine_item_id');
  var label_id = localStorage.getItem('label_id');
  var id_kapal = localStorage.getItem('id_kapal');

  var selbox = '<table class="table table-alpha">';
  var count = 0;
  console.log(url + "/machine/engine/sub/" + machinery_id + '/' + engine_item_id + '/' + label_id + '/' + engine_id + '/' + localStorage.getItem('id_kapal'));
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/machine/engine/sub/" + machinery_id + '/' + engine_item_id + '/' + label_id + '/' + engine_id + '/' + localStorage.getItem('id_kapal'), {} ).done( function( res ) {
    if(res.message == "success"){
      $.each( res.result, function(i, item) {
        if(count > 4){
          selbox += '<tr><td width="50%">' + i + '</td><td width="50%">: ' + item + '</td></tr>';
        } else if(i == 'e_i_s_id'){
          localStorage.setItem('e_i_s_id',item);
        }
        count++;
      } );
      selbox += '</table>';
      $('#list_item').html(selbox);
    } else {
      bisaSubmit = 0;
      alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
    }
    //SpinnerPlugin.activityStop();
    $('#myModal').modal('hide');
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
  if(localStorage.getItem('mode_report') == '1'){
    //aa
    // $("#smallImage").attr("src",localStorage.getItem('report_gambar'));
    // document.getElementById('smallImage').style.display = 'block';
    // localStorage.setItem('engine_item_id', engine_item_id);
    // localStorage.setItem('e_i_s_id', e_i_s_id);
    // localStorage.setItem('label_id', label_id);
    // localStorage.setItem('nama_label_engine',nama);
    if(localStorage.getItem('report_kondisi') == "-"){

    }
    else if(localStorage.getItem('report_kondisi') == "Good")
      $("#kondisi1").prop('checked', true);
    else
      $("#kondisi2").prop('checked', true);

    $('#suhu').val(localStorage.getItem('report_suhu'));
    $('#pressure').val(localStorage.getItem('report_pressure'));
    $('#keterangan').val(localStorage.getItem('report_keterangan'));
  }
}

function selectItem() {
  var kondisi = "";
  var kondisi = $("input[name='kondisi']:checked").val();
  var gambar = "";
  gambar = document.getElementById('smallImage').getAttribute("src");
  if(bisaSubmit){
    if($('#pressure').val() != "" && $('#suhu').val() != "" && kondisi != "" && gambar != "") {
      var dataToBeSent = {
        'pressure' : $('#pressure').val(),
        'suhu' : $('#suhu').val(),
        'kondisi' : kondisi,
        'keterangan' : $('#keterangan').val(),
        'id_survey' : localStorage.getItem('id_survey'),
        'vessel_imo' : localStorage.getItem('id_kapal'),
        'engine_id' : localStorage.getItem('machinery_id'),
        'engine_item_id' : localStorage.getItem('engine_item_id'),
        'engine_item_sub_id' : localStorage.getItem('label_id'),
        'engine' : localStorage.getItem('engine_id'),
        'e_i_s_id' : localStorage.getItem('e_i_s_id'),
        'gambar' : gambar
      };
      SpinnerPlugin.activityStart("Submitting Data...");
      $.post(url + "/machine", dataToBeSent, function(data, textStatus) {
        if(data.message == 'failed'){
          alert('Ada kesalahan data / jaringan!');
        } else {
          alert('data pengecekan berhasil terisi');
          window.location.href = "form_engine_next.html";
        }
        SpinnerPlugin.activityStop();
      }, "json");
    } else {
      alert('Data harus terisi semua!');
    }
  } else {
    alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
  }
  
}

function bantuan() {
  var bantuan = 'Data Belum diinput dari web';
  var id1 = localStorage.getItem('machinery_id');
  var id2 = localStorage.getItem('engine_item_id');
  var id3 = localStorage.getItem('label_id');
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  console.log( url + "/machine/bantuan/" + id1 + '/' + id2 + '/' + id3);
  $.getJSON( url + "/machine/bantuan/" + id1 + '/' + id2 + '/' + id3, {} ).done( function( resJob ) {
    if(resJob.message == 'failed'){
      $('#myModal').modal('hide');
      alert(bantuan);
    } else {
      // var count = 1;
      bantuan = "<ol>";
      $.each( resJob.result, function( listJob ) {
        // bantuan += count + '. ' + this.bantuan + '\n\n';
        bantuan += '<li>' + this.bantuan + '</li>';
        // count++;
      } );
    }
    bantuan += '</ol>';
    $('#contentModal').html(bantuan);
    $('#bantuanModal').modal('show');
    //SpinnerPlugin.activityStop();
    $('#myModal').modal('hide');
    // alert(bantuan);
  } ).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}