/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button1").addEventListener("click", goToHull);
  document.getElementById("button2").addEventListener("click", goToSuper);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function goToHull() {
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "menu_hull_report.html";
  else
    window.location.href = "menu_hull.html";
}

function goToSuper() {
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "menu_superstructure_report.html";
  else
    window.location.href = "menu_superstructure.html";
}

function back() {
  window.location.href = "survey_list.html";
}