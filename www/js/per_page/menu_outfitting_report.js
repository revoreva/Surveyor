/* global variables */
var url1 = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var url2 = "http://shipconditionsurvey.heliohost.org/surveykapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  window.location.href = "survey_list.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function show_image(url) {
  $("#imgModal").attr("src",url);
  // alert(url);
  // document.getElementById('imgModal').style.display = 'none';
  // document.getElementById('imgModal').style.display = 'block';
  // $('#imgModal').html("<img style=\"max-height:500px;max-width:500px;\" src=\""+ url + "\">");
  $('#gambarModal').modal('show');
}

function update(id, nama, kondisi, frame_start, frame_end, ketebalan, keterangan_tebal, outfitting_item_outfitting_id, keterangan, gambar) {
  localStorage.setItem('outfitting_id', id);
  localStorage.setItem('outfitting_nama',nama);
  localStorage.setItem('report_kondisi', kondisi);
  localStorage.setItem('report_frame_start', frame_start);
  localStorage.setItem('report_frame_end', frame_end);
  localStorage.setItem('report_ketebalan', ketebalan);
  localStorage.setItem('report_keterangan_tebal', keterangan_tebal);
  localStorage.setItem('report_outfitting_submit', outfitting_item_outfitting_id);
  localStorage.setItem('report_keterangan', keterangan);
  localStorage.setItem('report_gambar', gambar);
  window.location.href = "form_outfitting.html";
}

function getData() {
  $('#gambarModal').on('hidden', function () {
    $("#imgModal").attr("src",'img/loading.gif');
  })
  namama = "Safety Report";
  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);
  var id_kapal = localStorage.getItem('id_kapal');
  var id_survey = localStorage.getItem('id_survey_last');

  var selbox = '<table class="table table-alpha" style="width: 100%;"><tr>';
  selbox += '<th style="text-align:center">Item</th>';
  selbox += '<th style="text-align:center">Kondisi</th>';
  selbox += '<th style="text-align:center">Frame</th>';
  selbox += '<th style="text-align:center">Ketebalan</th>';
  selbox += '<th style="text-align:center">Keterangan</th>';
  selbox += '<th style="text-align:center">Gambar</th>';
  selbox += '<th style="text-align:center">Update</th>';
  selbox += '</tr>';
  var count = 0;
  var outfitting, benarsalah, rataval;
  $('#myModal').modal('show');
  $.ajax({
    type: "GET",
    url: url2 + "/report/outfitting_json/" + id_kapal + '/' + id_survey,
    dataType: "text",  
    cache:false,
    success: function (response) {
      outfitting = $.parseJSON(response);
      benarsalah = outfitting.benarsalah;
      rataval = outfitting.rataval;
      outfitting = outfitting.outfitting;
      console.log('success');
      console.log(outfitting);
      console.log(url2 + "/report/document_json/" + id_kapal + '/' + id_survey);
      $('#myModal').modal('hide');
    },
    error: function (ErrorResponse) {
      alert('Failed to Load Data from server!');
      outfitting = $.parseJSON('{"benarsalah":["-","-","-","salah","-","salah"],"rataval":[0,0,0,56,0,7],"outfitting":{"1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[],"8":[],"9":[],"10":[],"11":[],"12":[]}}');
      benarsalah = outfitting.benarsalah;
      rataval = outfitting.rataval;
      outfitting = outfitting.outfitting;
      console.log('error');
      console.log(outfitting);
      // outfitting = outfitting;
      $('#myModal').modal('hide');
    }
  }).done(function () {
    $.getJSON( url1 + "/outfitting", {
      } ).done( function( res ) {
        // console.log(res);
        if(res.message == "success"){
          var count = 1;
          $.each( res.result, function( i, item ) {
            var countL = 0;
            var id;
            var nama;
            $.each( this, function( j, item2 ) {
              if(countL == 0)
                id = item2;
              else if(countL == 1)
                nama = item2;
              countL++;
              //alert(selbox);
            } );
            selbox += '<tr><td width="16%">' + nama + '</td>';
            // console.log(outfitting[count].length);
            if(outfitting[count].length > 0)
            {
              // alert('ada');
              if (outfitting[count][0].outfitting_item_data_kondisi == 'Good') {
                selbox += '<td width="12%" align="center">' + outfitting[count][0].outfitting_item_data_kondisi + '</td>';
              } else {
                selbox += '<td bgcolor="#ff4d4d" width="12%" align="center">' + outfitting[count][0].outfitting_item_data_kondisi + '</td>';
              }
              selbox += '<td width="12%" align="center">' + outfitting[count][0].outfitting_item_data_frame_start + ' - ' + outfitting[count][0].outfitting_item_data_frame_end + '</td>';
              // selbox += '<td width="12%" align="center">' + outfitting[count][0].outfitting_item_data_ketebalan + '</td>';
              if(outfitting[count][0].outfitting_item_data_ketebalan == "0" )
                selbox += "<td>-</td>";
              else {
                if(benarsalah[0] == 'salah')
                selbox += "<td bgcolor='#ff4d4d'>" + outfitting[count][0].outfitting_item_data_ketebalan + "</td>";
                else selbox += "<td>" + outfitting[count][0].outfitting_item_data_ketebalan + "</td>";
              }
              /*if (outfitting[count][0].outfitting_data_status != 'Kadaluarsa') {
                selbox += '<td width="12%" align="center">' + outfitting[count][0].outfitting_data_status + '</td>';
              } else {
                selbox += '<td bgcolor="#ff4d4d" width="12%" align="center">' + outfitting[count][0].outfitting_data_status + '</td>';
              }*/
              selbox += '<td width="12%" align="center">' + outfitting[count][0].outfitting_item_data_keterangan + '</td>';
              selbox += "<td width='12%' onclick=\"show_image(\'" + outfitting[count][0].outfitting_item_data_gambar + "\')\" align=\"center\"><a href=\"#\">Lihat Gambar</a></td>";
              selbox += '<td width="12%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(';
              selbox += '\'' + id + '\', ';
              selbox += '\'' + nama + '\', ';
              selbox += '\'' + outfitting[count][0].outfitting_item_data_kondisi + '\', ';
              selbox += '\'' + outfitting[count][0].outfitting_item_data_frame_start + '\', ';
              selbox += '\'' + outfitting[count][0].outfitting_item_data_frame_end + '\', ';
              selbox += '\'' + outfitting[count][0].outfitting_item_data_ketebalan + '\', ';
              selbox += '\'' + outfitting[count][0].outfitting_item_data_keterangan_tebal + '\', ';
              selbox += '\'' + outfitting[count][0].outfitting_item_outfitting_id + '\', ';
              selbox += '\'' + outfitting[count][0].outfitting_item_data_keterangan + '\', ';
              selbox += '\'' + outfitting[count][0].outfitting_item_data_gambar + '\'';
              selbox += ')"></td>';
              //id, nama, kondisi, status, tanggal_start, tanggal_end, keterangan, gambar
            }
            else {
              // alert('tidak ada');
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
              selbox += '<td width="12%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(';
              selbox += '\'' + id + '\', ';
              selbox += '\'' + nama + '\', ';
              selbox += '\'-\', ';
              selbox += '\'-\', ';
              selbox += '\'-\', ';
              selbox += '\'-\', ';
              selbox += '\'-\', ';
              selbox += '\'-\', ';
              selbox += '\'-\', ';
              selbox += '\'-\'';
              selbox += ')"></td>';
              selbox += '</tr>';
            }
            // alert(selbox);
            count++;
          } );
          selbox += '</table>';
          $('#list_item').html("");
          $(selbox).appendTo('#list_item');
          // alert(selbox);
          $('#myModal').modal('hide');
          //SpinnerPlugin.activityStop();
        } else {
          alert('Failed to Load Data from server!');
          $('#myModal').modal('hide');
        }
      }).fail(function () {
        alert('Failed to Load Data from server!');
        $('#myModal').modal('hide');
      });
    });
}

function selectItem() {
  
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}