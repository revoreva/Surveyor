/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();

} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "safety_report.html";
  else
    window.location.href = "menu_navigations.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getData() {
  var safety_id = localStorage.getItem('safety_id');
  var id_kapal = localStorage.getItem('id_kapal');

  namama = localStorage.getItem('safety_nama');
  if(namama.length > 10){
    namama = namama.substring(0,10) + '..';
  }

  if((safety_id > 23 && safety_id < 28) ||
   (safety_id > 30 && safety_id < 34) || 
   safety_id == 35 || safety_id == 36 || 
   safety_id == 38){
    var element = document.getElementById("sdTanggal");
    element.parentNode.removeChild(element);
    element = document.getElementById("tanggalAkhir");
    element.parentNode.removeChild(element);
  } else if (safety_id == 28 || safety_id == 30){
    var element = document.getElementById("sdTanggal");
    element.parentNode.removeChild(element);
    element = document.getElementById("tanggalAkhir");
    element.parentNode.removeChild(element);
    element = document.getElementById("tanggalAwal");
    element.parentNode.removeChild(element);
    element = document.getElementById("labelTanggal");
    element.parentNode.removeChild(element);
  }

  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);

  var selbox = '<table class="table table-alpha">';
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/safety/" + safety_id + '/' + id_kapal, {} ).done( function( res ) {
    if(res.message == "success"){
      $.each( res.result, function() {
        $.each(this,function (j, item2) {
          if(j != 'item_id' && j != 'vessel_imo' && j != 'safety_id' && j != 'nas_id'){
            selbox += '<tr><td width="50%">' + j + '</td><td width="50%">' + item2 + '</td></tr>';
          }
        });
      } );
      selbox += '</table>';
      $('#list_item').html(selbox);
    } else {
      bisaSubmit = 0;
      alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
    }
    //SpinnerPlugin.activityStop();
    $('#myModal').modal('hide');
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function selectItem() {
  if(bisaSubmit){
    var safety_id = localStorage.getItem('safety_id');
    var kondisi = "";
    var kondisi = $("input[name='kondisi']:checked").val();
    var status = "";
    status = $("input[name='status']:checked").val();
    var date_start = "";
    var date_end = "";
    if((safety_id > 23 && safety_id < 28) ||
     (safety_id > 30 && safety_id < 34) || 
     safety_id == 35 || safety_id == 36 || 
     safety_id == 38){
      date_start = $('#bln_start_certificate').val() + '/' + $('#tgl_start_certificate').val() + '/' + $('#thn_start_certificate').val();
      date_end = date_start;
    } else if (safety_id == 28 || safety_id == 30){
      date_start = '01/01/1900';
      date_end = date_start;
    } else {
      date_start = $('#bln_start_certificate').val() + '/' + $('#tgl_start_certificate').val() + '/' + $('#thn_start_certificate').val();
      date_end = $('#bln_end_certificate').val() + '/' + $('#tgl_end_certificate').val() + '/' + $('#thn_end_certificate').val();
    }
    var gambar = "";
    gambar = document.getElementById('smallImage').getAttribute("src");
    if(kondisi != "" && status != "" && date_start != "" && date_end != "") {
      var dataToBeSent = {
        'kondisi' : kondisi,
        'start_date' : date_start,
        'end_date' : date_end,
        'status' : status,
        'vessel_imo' : localStorage.getItem('id_kapal'),
        'id_survey' : localStorage.getItem('id_survey'),
        'safety_id' : localStorage.getItem('safety_id'),
        'keterangan' : $('#keterangan').val(),
        'gambar' : gambar
      };
      SpinnerPlugin.activityStart("Submitting Data...");
      $.post(url + "/safety", dataToBeSent, function(data, textStatus) {
        if(data.message == 'failed'){
          alert('Ada kesalahan data / Jaringan!');
        } else {
          alert('data pengecekan berhasil terisi');
          window.location.href = "form_safety.html";
        }
        SpinnerPlugin.activityStop();
      }, "json");
    } else {
      alert('Data harus terisi semua!');
    }
  } else {
    alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
  }
}

function generateDate() {
  var today = new Date();

  var tglNow = today.getDate();
  var selbox = '<select class="sel form-control" id="tgl_start_certificate">';
  for(var tgl = 1; tgl <= 30; tgl++) selbox += '<option value="' + tgl + '" ' + (tgl == tglNow ? ' selected' : '') + '>' + tgl + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyStart');

  var blnNow = today.getMonth()+1;
  selbox = '<select class="sel form-control" id="bln_start_certificate">';
  var bulan = ['-','Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
  for(var bln = 1; bln <= 12; bln++) selbox += '<option value="' + bln + '" ' + (bln == blnNow ? ' selected' : '') + '>' + bulan[bln] + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyStart');

  var thnNow = today.getFullYear();
  selbox = '<select class="sel form-control" id="thn_start_certificate">';
  for(var thn = thnNow; thn >= 2000; thn--) selbox += '<option value="' + thn + '">' + thn + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyStart');

  selbox = '<select class="sel form-control" id="tgl_end_certificate">';
  for(var tgl = 1; tgl <= 30; tgl++) selbox += '<option value="' + tgl + '" ' + (tgl == tglNow ? ' selected' : '') + '>' + tgl + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyEnd');

  selbox = '<select class="sel form-control" id="bln_end_certificate">';
  var bulan = ['-','Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
  for(var bln = 1; bln <= 12; bln++) selbox += '<option value="' + bln + '" ' + (bln == blnNow ? ' selected' : '') + '>' + bulan[bln] + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyEnd');

  selbox = '<select class="sel form-control" id="thn_end_certificate">';
  for(var thn = thnNow; thn >= 2000; thn--) selbox += '<option value="' + thn + '">' + thn + '</option>';
  selbox += '</select>';
  $(selbox).appendTo('#dateSurveyEnd');

  if(localStorage.getItem('mode_report') == '1'){
    //aa
    // $("#smallImage").attr("src",localStorage.getItem('report_gambar'));
    // document.getElementById('smallImage').style.display = 'block';
    if(localStorage.getItem('report_kondisi') == "Good")
      $("#kondisi1").prop('checked', true);
    else
      $("#kondisi2").prop('checked', true);

    if(localStorage.getItem('report_status') == "Berlaku")
      $("#status1").prop('checked', true);
    else
      $("#status2").prop('checked', true);

    var report_tgl_start = localStorage.getItem('report_tanggal_start').split('-');
    var report_tgl_finish = localStorage.getItem('report_tanggal_finish').split('-');
    // console.log(Number(report_tgl_start[1]));
    console.log(bulan[Number(report_tgl_start[1])]);
    $('#tgl_start_certificate').val(report_tgl_start[2]);
    $('#bln_start_certificate').val(Number(report_tgl_start[1]));
    $('#thn_start_certificate').val(report_tgl_start[0]);

    $('#tgl_end_certificate').val(report_tgl_finish[2]);
    $('#bln_end_certificate').val(Number(report_tgl_finish[1]));
    $('#thn_end_certificate').val(report_tgl_finish[0]);
    $('#keterangan').val(localStorage.getItem('report_keterangan'));
  }
}

function bantuan() {
  var bantuan = 'Data Belum diinput dari web';
  var id1 = localStorage.getItem('safety_id');
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  $.getJSON( url + "/safety/bantuan/" + id1, {} ).done( function( resJob ) {
    if(resJob.message == 'failed'){
      $('#myModal').modal('hide');
      alert(bantuan);
    } else {
      // var count = 1;
      bantuan = "<ol>";
      $.each( resJob.result, function( listJob ) {
        // bantuan += count + '. ' + this.bantuan + '\n\n';
        bantuan += '<li>' + this.bantuan + '</li>';
        // count++;
      } );
    }
    bantuan += '</ol>';
    $('#contentModal').html(bantuan);
    $('#bantuanModal').modal('show');
    //SpinnerPlugin.activityStop();
    $('#myModal').modal('hide');
    // alert(bantuan);
  } ).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}