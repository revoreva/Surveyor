/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button1").addEventListener("click", goToCOR);
  document.getElementById("button2").addEventListener("click", goToHull);
  document.getElementById("button3").addEventListener("click", goToMachinery);
  document.getElementById("button4").addEventListener("click", goToOutFitting);
  document.getElementById("button5").addEventListener("click", goToNavigations);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function goToCOR() {
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "cor_report.html";
  else
    window.location.href = "cor.html";
}

function goToHull() {
  window.location.href = "pilihan_hull.html";
}

function goToMachinery() {
  window.location.href = "menu_machinery.html";
}

function goToOutFitting() {
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "menu_outfitting_report.html";
  else
    window.location.href = "menu_outfitting.html";
}

function goToNavigations() {
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "menu_navigations_report.html";
  else
    window.location.href = "menu_navigations.html";
}