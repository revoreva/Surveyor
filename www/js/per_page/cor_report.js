/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("back").addEventListener("click", back);
  // document.getElementById("button").addEventListener("click", next);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back(){
  window.location.href = "survey_list.html";
}

function logout() {
  //localStorage.removeItem('item');
  localStorage.clear();
  window.location.href = "index.html";
}

function next() {
  if($('#tipe_cer_doc').val() == '1')
    window.location.href = "certificate_report.html";
  else
    window.location.href = "document_report.html";
}