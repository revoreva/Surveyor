/* global variables */
var url1 = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var url2 = "http://shipconditionsurvey.heliohost.org/surveykapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  window.location.href = "menu_navigations_report.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function show_image(url) {
  $("#imgModal").attr("src",url);
  // alert(url);
  // document.getElementById('imgModal').style.display = 'none';
  // document.getElementById('imgModal').style.display = 'block';
  // $('#imgModal').html("<img style=\"max-height:500px;max-width:500px;\" src=\""+ url + "\">");
  $('#gambarModal').modal('show');
}

function update(id, nama, kondisi, status, tanggal_start, tanggal_end, keterangan, gambar) {
  localStorage.setItem('safety_id', id);
  localStorage.setItem('safety_nama',nama);
  localStorage.setItem('report_kondisi', kondisi);
  localStorage.setItem('report_status', status);
  localStorage.setItem('report_tanggal_start', tanggal_start);
  localStorage.setItem('report_tanggal_finish', tanggal_end);
  localStorage.setItem('report_keterangan', keterangan);
  localStorage.setItem('report_gambar', gambar);
  window.location.href = "form_safety.html";
}

function getData() {
  $('#gambarModal').on('hidden', function () {
    $("#imgModal").attr("src",'img/loading.gif');
  })
  namama = "Safety Report";
  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);
  var id_kapal = localStorage.getItem('id_kapal');
  var id_survey = localStorage.getItem('id_survey_last');

  var selbox = '<table class="table table-alpha" style="width: 100%;"><tr>';
  selbox += '<th style="text-align:center">Item</th>';
  selbox += '<th style="text-align:center">Kondisi</th>';
  selbox += '<th style="text-align:center">Status</th>';
  selbox += '<th style="text-align:center">Tanggal Berlaku</th>';
  selbox += '<th style="text-align:center">Keterangan</th>';
  selbox += '<th style="text-align:center">Gambar</th>';
  selbox += '<th style="text-align:center">Update</th>';
  selbox += '</tr>';
  var count = 0;
  var safety;
  $('#myModal').modal('show');
  $.ajax({
    type: "GET",
    url: url2 + "/report/safety_json/" + id_kapal + '/' + id_survey,
    dataType: "text",  
    cache:false,
    success: function (response) {
      safety = $.parseJSON(response);
      safety = safety.safety;
      // console.log('success');
      // console.log(safety);
      // console.log(url2 + "/report/document_json/" + id_kapal + '/' + id_survey);
      $('#myModal').modal('hide');
    },
    error: function (ErrorResponse) {
      alert('Failed to Load Data from server!');
      safety = $.parseJSON('{"safety":{"19":[],"20":[],"21":[],"22":[],"23":[],"24":[],"25":[],"26":[],"27":[],"28":[],"29":[],"30":[],"31":[],"32":[],"33":[],"34":[],"35":[],"36":[],"37":[],"38":[]}}');
      // console.log('error');
      // console.log(safety);
      // safety = safety;
      $('#myModal').modal('hide');
    }
  }).done(function () {
    $.getJSON( url1 + "/safety", {
      } ).done( function( res ) {
        // console.log(res);
        if(res.message == "success"){
          var count = 19;
          $.each( res.result, function( i, item ) {
            var countL = 0;
            var id;
            var nama;
            $.each( this, function( j, item2 ) {
              if(countL == 0)
                id = item2;
              else if(countL == 1)
                nama = item2;
              countL++;
              //alert(selbox);
            } );
            selbox += '<tr><td width="16%">' + nama + '</td>';
            // console.log(safety[count].length);
            if(safety[count].length > 0)
            {
              // alert('ada');
              if (safety[count][0].safety_data_kondisi == 'Good') {
                selbox += '<td width="12%" align="center">' + safety[count][0].safety_data_kondisi + '</td>';
              } else {
                selbox += '<td bgcolor="#ff4d4d" width="12%" align="center">' + safety[count][0].safety_data_kondisi + '</td>';
              }
              if (safety[count][0].safety_data_status != 'Kadaluarsa') {
                selbox += '<td width="12%" align="center">' + safety[count][0].safety_data_status + '</td>';
              } else {
                selbox += '<td bgcolor="#ff4d4d" width="12%" align="center">' + safety[count][0].safety_data_status + '</td>';
              }
              selbox += '<td width="12%" align="center">' + safety[count][0].safety_data_date_start + ' s.d ' + safety[count][0].safety_data_date_end + '</td>';
              selbox += '<td width="12%" align="center">' + safety[count][0].safety_data_keterangan + '</td>';
              selbox += "<td width='12%' onclick=\"show_image(\'" + safety[count][0].safety_data_gambar + "\')\" align=\"center\"><a href=\"#\">Lihat Gambar</a></td>";
              //id, nama, kondisi, status, tanggal_start, tanggal_end, keterangan, gambar
              selbox += '<td width="12%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(';
              selbox += '\'' + id + '\', ';
              selbox += '\'' + nama + '\', ';
              selbox += '\'' + safety[count][0].safety_data_kondisi + '\', ';
              selbox += '\'' + safety[count][0].safety_data_status + '\', ';
              selbox += '\'' + safety[count][0].safety_data_date_start + '\', ';
              selbox += '\'' + safety[count][0].safety_data_date_end + '\', ';
              selbox += '\'' + safety[count][0].safety_data_keterangan + '\', ';
              selbox += '\'' + safety[count][0].safety_data_gambar + '\'';
              selbox += ')"></td>';
            }
            else {
              // alert('tidak ada');
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
              selbox += '<td width="12%" align="center">Tidak ada data</td>';
            }

            selbox += '</tr>';
            // alert(selbox);
            count++;
          } );
          selbox += '</table>';
          $('#list_item').html("");
          $(selbox).appendTo('#list_item');
          // alert(selbox);
          $('#myModal').modal('hide');
          //SpinnerPlugin.activityStop();
        } else {
          alert('Failed to Load Data from server!');
          $('#myModal').modal('hide');
        }
      }).fail(function () {
        alert('Failed to Load Data from server!');
        $('#myModal').modal('hide');
      });
    });
}

function selectItem() {
  
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}