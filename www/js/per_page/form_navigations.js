/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();

} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function back() {
  if(localStorage.getItem('mode_report') == '1')
    window.location.href = "navigation_report.html";
  else
    window.location.href = "menu_navigations.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getData() {
  var navigations_id = localStorage.getItem('navigations_id');
  var id_kapal = localStorage.getItem('id_kapal');

  namama = localStorage.getItem('navigations_nama');
  if(namama.length > 17){
    namama = namama.substring(0,17) + '..';
  }

  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);

  var selbox = '<table class="table table-alpha">';
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  console.log(url + "/navigation/" + navigations_id + '/' + id_kapal);
  $.getJSON( url + "/navigation/" + navigations_id + '/' + id_kapal, {} ).done( function( res ) {
    if(res.message == "success"){
      $.each( res.result, function( ) {
        $.each(this,function (j,item2) {
          if(j != 'item_id' && j != 'vessel_imo' && j != 'navigations_id' && j != 'nas_id'){
            selbox += '<tr><td width="50%">' + j + '</td><td width="50%">' + item2 + '</td></tr>';
          }
        })
      } );
      selbox += '</table>';
      $('#list_item').html(selbox);
    } else {
      bisaSubmit = 0;
      alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
    }
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  }).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
  if(localStorage.getItem('mode_report') == '1'){
    //aa
    // $("#smallImage").attr("src",localStorage.getItem('report_gambar'));
    // document.getElementById('smallImage').style.display = 'block';
    if(localStorage.getItem('report_kondisi') == "Good")
      $("#kondisi1").prop('checked', true);
    else
      $("#kondisi2").prop('checked', true);

    $('#keterangan').val(localStorage.getItem('report_keterangan'));
  }
}

function selectItem() {
  if(bisaSubmit){
    var kondisi = "";
    var kondisi = $("input[name='kondisi']:checked").val();
    var gambar = document.getElementById('smallImage').getAttribute("src");
    if(kondisi != "") {
      var dataToBeSent = {
        'kondisi' : kondisi,
        'keterangan' : $('#keterangan').val(),
        'id_survey' : localStorage.getItem('id_survey'),
        'vessel_imo' : localStorage.getItem('id_kapal'),
        'navigation_id' : localStorage.getItem('navigations_id'),
        'gambar' : gambar
      };
      SpinnerPlugin.activityStart("Submitting Data...");
      $.post(url + "/navigation", dataToBeSent, function(data, textStatus) {
        if(data.message == 'failed'){
          alert('Ada kesalahan data / Kesalahan Jaringan!');
        } else {
          alert('data pengecekan berhasil terisi');
          window.location.href = "form_navigations.html";
        }
        SpinnerPlugin.activityStop();
      }, "json");
    } else {
      alert('Data harus terisi semua!');
    }
  } else {
    alert("Data belum ada. Admin harus mengisi data " + namama + " terlebih dahulu.");
  }
}

function bantuan() {
  var bantuan = 'Data Belum diinput dari web';
  var id1 = localStorage.getItem('navigations_id');
  //SpinnerPlugin.activityStart("Get Data...");
  $('#myModal').modal('show');
  console.log(url + "/navigation/bantuan/" + id1);
  $.getJSON( url + "/navigation/bantuan/" + id1, {} ).done( function( resJob ) {
    if(resJob.message == 'failed'){
      $('#myModal').modal('hide');
      alert(bantuan);
    } else {
      // var count = 1;
      bantuan = "<ol>";
      $.each( resJob.result, function( listJob ) {
        // bantuan += count + '. ' + this.bantuan + '\n\n';
        bantuan += '<li>' + this.bantuan + '</li>';
        // count++;
      } );
    }
    bantuan += '</ol>';
    $('#contentModal').html(bantuan);
    $('#bantuanModal').modal('show');
    //SpinnerPlugin.activityStop();
    $('#myModal').modal('hide');
    // alert(bantuan);
  } ).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}