/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("loginButton").addEventListener("click", login);
  document.getElementById("loginAdmin").addEventListener("click", goToAdmin);
} );

function refresh() {
  if(localStorage.getItem('username') != null){
    alert(localStorage.getItem('username') + ' was logged in');
    // if(localStorage.getItem('role') == 'admin')
    //   window.location.href = "select_kapal_admin.html";
    // else 
      window.location.href = "select_kapal.html";
  }
}

function goToAdmin() {
  window.location.href = "index.html";
}

function login() {
 	if ( $('#inputUsername').val() != "" && $('#inputPassword').val() != "" ) {
    var dataToBeSent = {
      'username' : $('#inputUsername').val(),
      'password' : $('#inputPassword').val()
    };
    SpinnerPlugin.activityStart("Login...");
    $.post(url + "/login", dataToBeSent, function(data, textStatus) {
      if(data.message == 'failed'){
        alert('Username / Password Anda salah!');
      } else {
        alert('Welcome ' + data.result.username);
        localStorage.setItem('username', data.result.username);
        localStorage.setItem('role','user');
        window.location.href = "select_kapal.html";
      }
      SpinnerPlugin.activityStop();
    }, "json");
  }
  else {
    alert('username dan password harus terisi!');
  }
}