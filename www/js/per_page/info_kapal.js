/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  } 
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function getKapal() {
  if(localStorage.getItem('mode_report') == '0')
    $('#footer').html('<button type="button" id="button2" onclick="goToVesselCrew()" class="btn btn-lg btn-primary btn-block"><span class="glyphicon glyphicon-user"></span> Vessel Crew</button>');
  var id = localStorage.getItem('id_kapal');
  $('#myModal').modal('show');
  $.getJSON( url + "/kapal/" + id, {
  } ).done( function( res ) {
    //SpinnerPlugin.activityStart("Mengunduh informasi kapal...");
    var str = '<table class="table table-alpha">';
    $.each( res.result, function( j, item2 ) {
      if(j != 'id_kapal'){
        str += '<tr>';
        str += '<td width="50%">' + j + '</td><td width="50%">: ' + item2 + '</td>';
        str += '</tr>';
      }
    } );
    str += '<tr>';
    str += '<td width="50%">Date of Survey</td><td width="50%">: ' + localStorage.getItem('date'); + '</td>';
    str += '</tr>';

    str += '<tr>';
    str += '<td width="50%">Survey of Company</td><td width="50%">: ' + localStorage.getItem('company'); + '</td>';
    str += '</tr>';

    str += '<tr>';
    str += '<td width="50%">Name of Surveyor</td><td width="50%">: ' + localStorage.getItem('surveyor'); + '</td>';
    str += '</tr>';
    str += '</table>';

    $('#detail_kapal').html("");
    $(str).appendTo('#detail_kapal');
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  } ).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function goToVesselCrew() {
  window.location.href = "vessel_crew.html";
}

function goToListSurveyReport() {
  window.location.href = "list_survey_report.html";
}