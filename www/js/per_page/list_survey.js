/* global variables */
var url = "http://shipconditionsurvey.heliohost.org/kapal/index.php";

/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
  document.getElementById("button").addEventListener("click", submitSurvey);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  }
  // if(localStorage.getItem('role') == 'admin'){
  //   alert('you must logged in as user');
  //   window.location.href = "index.html";
  // }
}

function back(){
  window.location.href = "buat_survey.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function submitSurvey() {
  var tanggal = "", company = "", surveyor = "", ada = 0;
  var id_survey = $('#id_survey').val();
  localStorage.setItem('id_survey',id_survey);
  $('#myModal').modal('show');
  $.getJSON( url + "/survey/" + localStorage.getItem('id_kapal'), {
  } ).done( function( res ) {
    //SpinnerPlugin.activityStart("Mengunduh informasi kapal...");
    $.each( res.result, function(a) {
      if(res.message == 'success'){
        $.each( this, function( j, item2 ) {
          if(j == 'id_survey' && item2 == id_survey && ada == 0){
            ada = 1;
          }
          if((tanggal == "" || surveyor == "" || company == "") && ada == 1){
            if(j == 'tanggal') tanggal = item2;
            else if(j == 'surveyor') surveyor = item2;
            else if(j == 'survey_company') company = item2;
            console.log(tanggal + ' ' + surveyor + ' ' + company + ' ' + ada);
          }
        } );
        if(ada){
          localStorage.setItem('date',tanggal);
          localStorage.setItem('company',company);
          localStorage.setItem('surveyor',surveyor);
          window.location.href = "info_kapal.html";
        }
      }
    } );
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  } ).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}

function generateData() {
  var id_kapal = localStorage.getItem('id_kapal');
  $('#myModal').modal('show');
  console.log(url + "/survey/" + id_kapal);
  $.getJSON( url + "/survey/" + id_kapal, {
  } ).done( function( res ) {
    //SpinnerPlugin.activityStart("Mengunduh informasi kapal...");
    var str = '<select class="form-control" id="id_survey">';
    var ada = 1;
    $.each( res.result, function(a) {
      var id_survey, tanggal, surveyor, survey_company;
      if(res.message == 'success'){
        $.each( this, function( j, item2 ) {
          if(j == 'id_survey') id_survey = item2;
          else if(j == 'tanggal') tanggal = item2;
          else if(j == 'surveyor') surveyor = item2;
          else if(j == 'survey_company') survey_company = item2;
        } );
      }
      else {
        ada = 0;
      }
      str += '<option value="' + id_survey + '">' + tanggal + ', Survey Company: ' + survey_company + '</option>';
    } );
    str += '</select>';
    if(ada){
      $('#list_survey').html(str);
    } else {
      alert("Data tidak ada");
    }
    $('#myModal').modal('hide');
    //SpinnerPlugin.activityStop();
  } ).fail(function () {
    alert('Failed to Load Data from server!');
    $('#myModal').modal('hide');
  });
}