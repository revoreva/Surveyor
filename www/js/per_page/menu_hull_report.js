/* global variables */
var url1 = "http://shipconditionsurvey.heliohost.org/kapal/index.php";
var url2 = "http://shipconditionsurvey.heliohost.org/surveykapal/index.php";
var bisaSubmit = 1;
var namama;
/* initializer */
$( document ).ready( function() {
  refresh();
  document.getElementById("logout").addEventListener("click", logout);
} );

function refresh() {
  if(localStorage.getItem('username') == null){
    alert('You must logged in first');
    window.location.href = "index.html";
  }
  localStorage.setItem('last_hull','hull');
}

function back() {
  window.location.href = "pilihan_hull.html";
}

function logout() {
  localStorage.clear();
  window.location.href = "index.html";
}

function show_image(url) {
  $("#imgModal").attr("src",url);
  // alert(url);
  // document.getElementById('imgModal').style.display = 'none';
  // document.getElementById('imgModal').style.display = 'block';
  // $('#imgModal').html("<img style=\"max-height:500px;max-width:500px;\" src=\""+ url + "\">");
  $('#gambarModal').modal('show');
}

function view_tebal_normal(min, max) {
  alert('Tebal normal: ' + min + ' - ' + max);
}

function update(id, nama, kondisi, frame_start, frame_end, lajur, ketebalan, keterangan_tebal, hull_item_hull_id, keterangan, gambar) {
  // alert(id + ' ' + nama + ' ' + kondisi);
  localStorage.setItem('hull_id', id);
  localStorage.setItem('hull_nama',nama);
  localStorage.setItem('report_kondisi', kondisi);
  localStorage.setItem('report_frame_start', frame_start);
  localStorage.setItem('report_frame_end', frame_end);
  localStorage.setItem('report_lajur', lajur);
  localStorage.setItem('report_ketebalan', ketebalan);
  localStorage.setItem('report_keterangan_tebal', keterangan_tebal);
  localStorage.setItem('report_hull_item_id', hull_item_hull_id);
  localStorage.setItem('report_keterangan', keterangan);
  localStorage.setItem('report_gambar', gambar);
  window.location.href = "form_hull_item.html";
}

function getData() {
  $('#gambarModal').on('hidden', function () {
    $("#imgModal").attr("src",'img/loading.gif');
  })
  namama = "Hull Report";
  $('#nama_engine_item').html('<button id="back" type="button" class="btn btn-default" onclick="back()"><span class="button glyphicon glyphicon-triangle-left"></span></button> ' + namama);
  var id_kapal = localStorage.getItem('id_kapal');
  var id_survey = localStorage.getItem('id_survey_last');

  var selbox = '<div class="table table-alpha" style="width: 100%;">';
  var count = 0;
  var hull, hullyeah, label, parameter, benarsalah, rataval, tebalmin, tebalmax;
  $('#myModal').modal('show');
  $.ajax({
    type: "GET",
    url: url2 + "/report/hull_json/" + id_kapal + '/' + id_survey,
    dataType: "text",  
    cache:false,
    success: function (response) {
      hull = $.parseJSON(response);
      hullyeah = hull.hullyeah;
      rataval = hull.rataval;
      tebalmin = hull.tebalmin;
      tebalmax = hull.tebalmax;
      label = hull.label;
      console.log(label);
      parameter = hull.parameter;
      benarsalah = hull.benarsalah;
      hull = hull.hull;
      console.log('success');
      // console.log(parameter);
      console.log(hull);
      console.log(url2 + "/report/hull_json/" + id_kapal + '/' + id_survey);
      $('#myModal').modal('hide');
    },
    error: function (ErrorResponse) {
      alert('Failed to Load Data from server!');
      hull = $.parseJSON('{"hullyeah":["-","-","-","-","-","-"],"label":[0,0,0,0,0,0],"hull":{"1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[]}}');
      hullyeah = hull.hullyeah;
      benarsalah = hull.benarsalah;
      tebalmin = hull.tebalmin;
      tebalmax = hull.tebalmax;
      rataval = hull.rataval;
      label = hull.label;
      parameter = hull.parameter;
      hull = hull.hull;
      console.log('error');
      console.log(hull);
      // hull = hull;
      $('#myModal').modal('hide');
    }
  }).done(function () {
    $.getJSON( url1 + "/hull/hull", {
      } ).done( function( res ) {
        // console.log(res);
        if(res.message == "success"){
          var countHull = 0;
          var count = 1;
          var konter = 0;
          selbox += '<ul class="nav nav-tabs">';
          var awal = 1;
          $.each( res.result, function( i, item ) {
            var countLL = 0;
            var id;
            var nama;
            $.each( this, function( j, item2 ) {
              if(countLL == 1)
                id = item2;
              else if(countLL == 2)
                nama = item2;
              countLL++;
              //alert(selbox);
            } );
            // var nama_asli = nama;
            // nama = nama.replace(" ", "_");
            selbox += '<li';
            if(awal){
              selbox += ' class="active"';
              awal = 0;
            }
            selbox += '><a data-toggle="tab" href="#' + nama.replace(" ", "_") + '">' + nama + '</a></li>';            count++;
          } );
          selbox += '</ul>';
          selbox += '<div class="tab-content" style="width:100%;">';
          awal = 1;
          count = 0;
          $.each( res.result, function( i, item ) 
          {
            var countLL = 0;
            var id;
            var nama;
            $.each( this, function( j, item2 ) {
              if(countLL == 1)
                id = item2;
              else if(countLL == 2)
                nama = item2;
              countLL++;
              //alert(selbox);
            } );
            nama = nama.replace(" ", "_");
            selbox += '<div id="' + nama + '" class="tab-pane fade in';
            if(awal){
              selbox += ' active';
              awal = 0;
            }
            selbox += '" style="width:100%;">';
            // selbox += '<h3>' + nama + '</h3>';
            // selbox += '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>';
            var sizeLabel = label[count].length;
            selbox += '<table class="table table-alpha">';
            selbox += '<tr>';
            selbox += '<th style="text-align:center">Item</th>';
            selbox += '<th style="text-align:center">Kondisi</th>';
            selbox += '<th style="text-align:center">Frame</th>';
            selbox += '<th style="text-align:center">Lajur</th>';
            selbox += '<th style="text-align:center">Ketebalan</th>';
            selbox += '<th style="text-align:center">Keterangan</th>';
            selbox += '<th style="text-align:center">Gambar</th>';
            selbox += '<th style="text-align:center">Update</th>';
            selbox += '</tr>';
            console.log('label');
            console.log(label[count]);

            for(var i = 0; i < sizeLabel; i++){
              var flag = 0;
              selbox += '<tr>';
              selbox += '<td';
              if(hull[countHull].length > 0){
                selbox += ' rowspan="' + hull[countHull].length + '"';
              }
              selbox += '>' + label[count][i].hull_item_nama + '</td>';
              //for tiap data
              if(hull[countHull].length > 0){
                for(var j = 0; j < hull[countHull].length; j++){
                  if(!flag){
                    // selbox += count;
                    flag = 1;
                  } else {
                    selbox += "<tr>";
                  }
                  if(hull[countHull][j].h_i_d_kondisi!="Good")
                    selbox += "<td bgcolor='#ff4d4d' align='center' style='vertical-align:middle;'>"/* + countHull + '----'*/ + hull[countHull][j].h_i_d_kondisi + "</td>";
                  else 
                    selbox += "<td align='center' style='vertical-align:middle;'>" + hull[countHull][j].h_i_d_kondisi + "</td>";

                  selbox += "<td align='center' style='vertical-align:middle;'>" + hull[countHull][j].h_i_d_frame_start + " - " + hull[countHull][j].h_i_d_frame_end + "</td>";
                  selbox += "<td align='center' style='vertical-align:middle;'>" + hull[countHull][j].h_i_d_lajur + '</td>';
                  if(benarsalah[konter] == "salah")
                  {
                    selbox += "<td onclick='view_tebal_normal(" + tebalmin[konter] + ", " + tebalmax[konter] + ")' align='center' style='vertical-align:middle;' bgcolor='#ff4d4d'><a href='#' style='color: black; text-decoration: underline;'>" + parseFloat(rataval[konter]).toFixed(2) + "</a></td>";
                    konter++;
                  }
                  else {
                    selbox += "<td align='center' style='vertical-align:middle;'>" + parseFloat(rataval[konter]).toFixed(2) + "</td>";
                    konter++;
                  }

                  selbox += "<td align='center' style='vertical-align:middle;'>" + hull[countHull][j].h_i_d_keterangan + "</td>";

                  selbox += "<td style='text-align:center; vertical-align:middle;' onclick=\"show_image(\'" + hull[countHull][j].h_i_d_gambar + "\')\" align=\"center\"><a href=\"#\">Lihat Gambar</a></td>";
                  //id, nama, kondisi, frame_start, frame_end, lajur, ketebalan, keterangan_tebal, hull_item_hull_id, keterangan, gambar
                  selbox += '<td width="12%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(';
                  selbox += '\'' + id + '\', ';
                  selbox += '\'' + label[count][i].hull_item_nama + '\', ';
                  selbox += '\'' + hull[countHull][j].h_i_d_kondisi + '\', ';
                  selbox += '\'' + hull[countHull][j].h_i_d_frame_start + '\', ';
                  selbox += '\'' + hull[countHull][j].h_i_d_frame_end + '\', ';
                  selbox += '\'' + hull[countHull][j].h_i_d_lajur + '\', ';
                  selbox += '\'' + parseFloat(rataval[konter]).toFixed(2) + '\', ';
                  selbox += '\'' + hull[countHull][j].h_i_d_keterangan_tebal + '\', ';
                  // console.log('--------start');
                  // console.log(label[count]);
                  // console.log(label[count][i]);
                  // console.log('--------end');
                  selbox += '\'' + label[count][i].hull_item_id + '\', ';
                  selbox += '\'' + hull[countHull][j].h_i_d_keterangan + '\', ';
                  selbox += '\'' + hull[countHull][j].h_i_d_gambar + '\'';
                  selbox += ')"></td>';
                  // selbox += '</tr>';
                }
              } else {
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td style="text-align:center; vertical-align:middle;">Tidak ada data</td>';
                selbox += '<td width="12%" align="center"><input type="submit" class="btn btn-primary" value="update" onclick="update(';
                //id, nama, kondisi, frame_start, frame_end, lajur, ketebalan, keterangan_tebal, hull_item_hull_id, keterangan, gambar
                selbox += '\'' + id + '\', ';
                selbox += '\'' + label[count][i].hull_item_nama + '\', ';
                selbox += '\'-\', ';
                selbox += '\'-\', ';
                selbox += '\'-\', ';
                selbox += '\'-\', ';
                selbox += '\'-\', ';
                selbox += '\'-\', ';
                selbox += '\'' + label[count][i].hull_item_id + '\', ';
                selbox += '\'-\', ';
                selbox += '\'-\'';
                selbox += ')"></td>';
              }
              countHull++;
              selbox += '</tr>';
            }
            selbox += '</table>';
            selbox += '</div>';
            count++;
          });
          selbox += '</div>';
          // selbox += '</td></tr>';
          selbox += '</div>';
          // selbox += '<tr><th style="text-align:center">Item</th>';
          // selbox += '<th style="text-align:center">Kondisi</th>';
          // selbox += '<th style="text-align:center">Frame</th>';
          // selbox += '<th style="text-align:center">Ketebalan</th>';
          // selbox += '<th style="text-align:center">Keterangan</th>';
          // selbox += '<th style="text-align:center">Gambar</th>';
          // selbox += '<th style="text-align:center">Update</th>';
          // selbox += '</tr>';
          $('#list_item').html("");
          $(selbox).appendTo('#list_item');
          // alert(selbox);
          $('#myModal').modal('hide');
          //SpinnerPlugin.activityStop();
        } else {
          alert('Failed to Load Data from server!');
          $('#myModal').modal('hide');
        }
      }).fail(function () {
        alert('Failed to Load Data from server!');
        $('#myModal').modal('hide');
      });
    });
}

function selectItem() {
  
}

function goToSurveyList() {
  window.location.href = "survey_list.html";
}